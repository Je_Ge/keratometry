<?xml version="1.0" encoding="ISO-8859-1"?>
<febio_spec version="2.5">
	<Module type="biphasic"/>
	<Include>parameters.feb</Include>
	<Globals>
		<Constants>
			<T>0</T>
			<R>0</R>
			<Fc>0</Fc>
		</Constants>
	</Globals>
	<Material>
		<material id="1" name="Material_stroma" type="biphasic">
			<phi0>0.2</phi0>
			<fluid_density>1000</fluid_density>
			<solid type="neo-Hookean">
				<density>1</density>
				<E>@young_stroma</E>
				<v>@poison_stroma</v>
			</solid>
			<permeability type="perm-Holmes-Mow">
				<perm>@permeability_stroma</perm>
				<M>@M_stroma</M>
				<alpha>@alpha_stroma</alpha>
			</permeability>
		</material>
		<material id="2" name="Material_epithelium" type="biphasic">
			<phi0>0.2</phi0>
			<fluid_density>1000</fluid_density>
			<solid type="neo-Hookean">
				<density>1</density>
				<E>@young_epi</E>
				<v>@poison_epi</v>
			</solid>
			<permeability type="perm-Holmes-Mow">
				<perm>@permeability_epi</perm>
				<M>@M_epi</M>
				<alpha>@alpha_epi</alpha>
			</permeability>
		</material>
		<material id="3" name="Material_lens" type="rigid body">
			<density>1</density>
			<center_of_mass>0,0,0</center_of_mass>
		</material>
	</Material>
		<Geometry from="abaqus_mesh.feb"/>
	<Boundary>
		<fix bc="z" node_set="sym_z_BC"/>
		<fix bc="x,z" node_set="axis_BC"/>
		<fix bc="x,y" node_set="intraocular_pres_BC"/>
		<rigid_body mat="3">
			<fixed bc="x"/>
			<fixed bc="z"/>
			<fixed bc="Rx"/>
			<fixed bc="Ry"/>
			<fixed bc="Rz"/>
		</rigid_body>
	</Boundary>
	<Contact>
		<contact type="sliding-biphasic" name="BiphasicContact1" surface_pair="BiphasicContact1">
			<laugon>1</laugon>
			<tolerance>0.2</tolerance>
			<gaptol>0</gaptol>
			<ptol>0</ptol>
			<penalty>1</penalty>
			<two_pass>0</two_pass>
			<auto_penalty>0</auto_penalty>
			<pressure_penalty>0</pressure_penalty>
			<symmetric_stiffness>0</symmetric_stiffness>
			<search_radius>1</search_radius>
			<seg_up>0</seg_up>
			<minaug>0</minaug>
			<maxaug>10</maxaug>
			<search_tol>0.01</search_tol>
		</contact>
	</Contact>
	<Constraints>
		<constraint type="symmetry plane" name="SymmetryPlane1" surface="SymmetryPlane1">
			<laugon>1</laugon>
			<tol>0.2</tol>
			<penalty>1</penalty>
			<minaug>0</minaug>
			<maxaug>0</maxaug>
		</constraint>
	</Constraints>
	<LoadData>
		<loadcurve id="1" type="smooth" extend="constant">
			<point>0,0</point>
			<point>90,1</point>
		</loadcurve>
		<loadcurve id="2" type="smooth" extend="constant">
			<point>0,0.25</point>
			<point>90,1</point>
		</loadcurve>		
	</LoadData>
	<Include>unload_curve.feb</Include>
	<Include>must_point_curve_1.feb</Include>
	<Include>must_point_curve_2.feb</Include>
	<Output>
		<plotfile type="febio">
			<var type="displacement"/>
			<var type="stress"/>
			<var type="effective fluid pressure"/>
			<var type="fluid flux"/>
			<var type="volume fraction"/>
			<var type="relative volume"/>
			<var type="rigid force"/>
		</plotfile>
		<logfile>
                        <node_data data="ux;uy;uz" delim="," file="anterior_surface_displacement.dat" node_set="anterior_surface"/>
		</logfile>
	</Output>
	<Step name="Step0">
		<Control>
			<restart file="restart_out">1</restart>
			<time_steps>100</time_steps>
			<step_size>0.1</step_size>
			<max_refs>50</max_refs>
			<max_ups>25</max_ups>
			<diverge_reform>1</diverge_reform>
			<reform_each_time_step>1</reform_each_time_step>
			<dtol>0.001</dtol>
			<etol>0.01</etol>
			<rtol>0</rtol>
			<ptol>0.01</ptol>
			<lstol>0.9</lstol>
			<min_residual>1e-20</min_residual>
			<qnmethod>0</qnmethod>
			<time_stepper>
				<dtmin>0.000001</dtmin>
				<dtmax>1</dtmax>
				<max_retries>45</max_retries>
				<opt_iter>15</opt_iter>
			</time_stepper>
			<symmetric_biphasic>0</symmetric_biphasic>
		</Control>
		<Boundary>
			<rigid_body mat="3">
				<prescribed bc="y" lc="1">-0.001</prescribed>
			</rigid_body>
		</Boundary>
		<Constraints>
		</Constraints>
	</Step>
	<Step name="Step1">
		<Control>
			<restart file="restart_out">1</restart>
			<time_steps>287900</time_steps>
			<step_size>0.1</step_size>
			<max_refs>50</max_refs>
			<max_ups>25</max_ups>
			<diverge_reform>1</diverge_reform>
			<reform_each_time_step>1</reform_each_time_step>
			<dtol>0.001</dtol>
			<etol>0.01</etol>
			<rtol>0</rtol>
			<ptol>0.01</ptol>
			<lstol>0.9</lstol>
			<min_residual>1e-20</min_residual>
			<qnmethod>0</qnmethod>
			<time_stepper>
				<dtmin>0.000001</dtmin>
				<dtmax lc="4">1</dtmax>
				<max_retries>45</max_retries>
				<opt_iter>15</opt_iter>
			</time_stepper>
			<symmetric_biphasic>0</symmetric_biphasic>
		</Control>
		<Boundary>
			<rigid_body mat="3">
				<force bc="y" lc="2">-0.0006667</force>
			</rigid_body>
		</Boundary>
		<Constraints>
		</Constraints>
	</Step>
	<Step name="Step2">
		<Control>
			<time_steps>432000</time_steps>
			<step_size>0.1</step_size>
			<max_refs>50</max_refs>
			<max_ups>30</max_ups>
			<diverge_reform>1</diverge_reform>
			<reform_each_time_step>1</reform_each_time_step>
			<dtol>0.001</dtol>
			<etol>0.01</etol>
			<rtol>0</rtol>
			<ptol>0.01</ptol>
			<lstol>0.9</lstol>
			<min_residual>1e-20</min_residual>
			<qnmethod>0</qnmethod>
			<time_stepper>
				<dtmin>0.00001</dtmin>
				<dtmax lc="5">1</dtmax>
				<max_retries>45</max_retries>
				<opt_iter>15</opt_iter>
			</time_stepper>
			<symmetric_biphasic>0</symmetric_biphasic>
		</Control>
		<Boundary>
			<rigid_body mat="3">
				<prescribed bc="y" lc="3">1</prescribed>
			</rigid_body>
		</Boundary>
	</Step>
</febio_spec>
