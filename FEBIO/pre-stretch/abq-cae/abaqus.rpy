# -*- coding: mbcs -*-
#
# Abaqus/CAE Release 6.13-5 replay file
# Internal Version: 2018_09_24-18.33.52 127000
# Run by ragnarok on Mon Sep 23 10:47:29 2019
#

# from driverUtils import executeOnCaeGraphicsStartup
# executeOnCaeGraphicsStartup()
#: Executing "onCaeGraphicsStartup()" in the site directory ...
from abaqus import *
from abaqusConstants import *
session.Viewport(name='Viewport: 1', origin=(0.0, 0.0), width=338.402069091797, 
    height=170.395004272461)
session.viewports['Viewport: 1'].makeCurrent()
session.viewports['Viewport: 1'].maximize()
from caeModules import *
from driverUtils import executeOnCaeStartup
executeOnCaeStartup()
Mdb()
#: A new model database has been created.
#: The model "Model-1" has been created.
session.viewports['Viewport: 1'].setValues(displayedObject=None)
s = mdb.models['Model-1'].ConstrainedSketch(name='__profile__', 
    sheetSize=200.0)
g, v, d, c = s.geometry, s.vertices, s.dimensions, s.constraints
s.setPrimaryObject(option=STANDALONE)
s.rectangle(point1=(-2.5, -20.0), point2=(2.5, 20.0))
p = mdb.models['Model-1'].Part(name='Part-1', dimensionality=THREE_D, 
    type=DEFORMABLE_BODY)
p = mdb.models['Model-1'].parts['Part-1']
p.BaseSolidExtrude(sketch=s, depth=1.0)
s.unsetPrimaryObject()
p = mdb.models['Model-1'].parts['Part-1']
session.viewports['Viewport: 1'].setValues(displayedObject=p)
del mdb.models['Model-1'].sketches['__profile__']
p = mdb.models['Model-1'].parts['Part-1']
c = p.cells
pickedCells = c.getSequenceFromMask(mask=('[#1 ]', ), )
v1, e, d1 = p.vertices, p.edges, p.datums
p.PartitionCellByPlaneThreePoints(cells=pickedCells, point1=p.InterestingPoint(
    edge=e[4], rule=MIDDLE), point2=p.InterestingPoint(edge=e[6], rule=MIDDLE), 
    point3=p.InterestingPoint(edge=e[10], rule=MIDDLE))
p = mdb.models['Model-1'].parts['Part-1']
c = p.cells
pickedCells = c.getSequenceFromMask(mask=('[#3 ]', ), )
v2, e1, d2 = p.vertices, p.edges, p.datums
p.PartitionCellByPlaneThreePoints(cells=pickedCells, point1=p.InterestingPoint(
    edge=e1[0], rule=MIDDLE), point2=p.InterestingPoint(edge=e1[5], 
    rule=MIDDLE), point3=p.InterestingPoint(edge=e1[18], rule=MIDDLE))
p = mdb.models['Model-1'].parts['Part-1']
f, e, d1 = p.faces, p.edges, p.datums
t = p.MakeSketchTransform(sketchPlane=f[4], sketchUpEdge=e[25], 
    sketchPlaneSide=SIDE1, origin=(-1.25, 10.0, 1.0))
s1 = mdb.models['Model-1'].ConstrainedSketch(name='__profile__', 
    sheetSize=40.36, gridSpacing=1.0, transform=t)
g, v, d, c = s1.geometry, s1.vertices, s1.dimensions, s1.constraints
s1.setPrimaryObject(option=SUPERIMPOSE)
p = mdb.models['Model-1'].parts['Part-1']
p.projectReferencesOntoSketch(sketch=s1, filter=COPLANAR_EDGES)
session.viewports['Viewport: 1'].view.setValues(nearPlane=38.2955, 
    farPlane=43.3271, width=23.4517, height=13.8556, cameraPosition=(1.51361, 
    16.2301, 41.3113), cameraTarget=(1.51361, 16.2301, 1))
s1.ConstructionLine(point1=(0.5, 9.75), point2=(0.5, 9.0))
s1.VerticalConstraint(entity=g[18], addUndoState=False)
s1.ConstructionLine(point1=(2.0, 9.5), point2=(2.0, 8.75))
s1.VerticalConstraint(entity=g[19], addUndoState=False)
s1.DistanceDimension(entity1=g[6], entity2=g[18], textPoint=(0.60618782043457, 
    7.97979545593262), value=0.0)
s1.undo()
s1.DistanceDimension(entity1=g[18], entity2=g[6], textPoint=(0.902627944946289, 
    7.78171920776367), value=0.5)
s1.DistanceDimension(entity1=g[6], entity2=g[19], textPoint=(1.80841636657715, 
    6.62627029418945), value=0.5)
session.viewports['Viewport: 1'].view.setValues(nearPlane=37.6633, 
    farPlane=43.9593, width=22.258, height=13.1504, cameraPosition=(1.394, 
    2.03114, 41.3113), cameraTarget=(1.394, 2.03114, 1))
s1.ConstructionLine(point1=(0.25, -9.5), point2=(-1.0, -9.5))
s1.HorizontalConstraint(entity=g[20], addUndoState=False)
s1.ConstructionLine(point1=(0.25, -10.5), point2=(-1.25, -10.5))
s1.HorizontalConstraint(entity=g[21], addUndoState=False)
s1.DistanceDimension(entity1=g[20], entity2=g[2], textPoint=(-1.88889265060425, 
    -9.89580988883972), value=0.5)
s1.DistanceDimension(entity1=g[2], entity2=g[21], textPoint=(-2.07646036148071, 
    -10.4754621982574), value=0.5)
session.viewports['Viewport: 1'].view.setValues(nearPlane=28.8786, 
    farPlane=52.744, width=96.0985, height=56.7766, cameraPosition=(24.678, 
    4.11409, 41.3113), cameraTarget=(24.678, 4.11409, 1))
s1.Line(point1=(0.75, 12.5), point2=(0.75, -32.5))
s1.VerticalConstraint(entity=g[22], addUndoState=False)
s1.ParallelConstraint(entity1=g[18], entity2=g[22], addUndoState=False)
s1.CoincidentConstraint(entity1=v[9], entity2=g[18], addUndoState=False)
session.viewports['Viewport: 1'].view.setValues(nearPlane=32.4716, 
    farPlane=49.151, width=65.8973, height=38.9332, cameraPosition=(16.9156, 
    -3.89419, 41.3113), cameraTarget=(16.9156, -3.89419, 1))
s1.Line(point1=(1.75, 12.5), point2=(2.0, -32.25))
s1.CoincidentConstraint(entity1=v[11], entity2=g[19], addUndoState=False)
s1.VerticalConstraint(entity=g[23])
session.viewports['Viewport: 1'].view.setValues(nearPlane=38.4371, 
    farPlane=43.1855, width=15.7535, height=9.30745, cameraPosition=(0.695918, 
    0.685912, 41.3113), cameraTarget=(0.695918, 0.685912, 1))
s1.Line(point1=(-2.39072704315186, -9.5), point2=(4.52356767654419, -9.5))
s1.HorizontalConstraint(entity=g[24], addUndoState=False)
s1.ParallelConstraint(entity1=g[20], entity2=g[24], addUndoState=False)
s1.CoincidentConstraint(entity1=v[13], entity2=g[20], addUndoState=False)
s1.CoincidentConstraint(entity1=v[14], entity2=g[20], addUndoState=False)
s1.Line(point1=(-2.5, -10.5), point2=(5.0545859336853, -10.5))
s1.HorizontalConstraint(entity=g[25], addUndoState=False)
s1.ParallelConstraint(entity1=g[21], entity2=g[25], addUndoState=False)
s1.CoincidentConstraint(entity1=v[15], entity2=g[21], addUndoState=False)
p = mdb.models['Model-1'].parts['Part-1']
f = p.faces
pickedFaces = f.getSequenceFromMask(mask=('[#d57ff ]', ), )
f1, e1, d2 = p.faces, p.edges, p.datums
p.PartitionFaceBySketchThruAll(sketchPlane=f1[4], sketchUpEdge=e1[25], 
    faces=pickedFaces, sketchPlaneSide=SIDE1, sketch=s1)
s1.unsetPrimaryObject()
del mdb.models['Model-1'].sketches['__profile__']
session.viewports['Viewport: 1'].view.setValues(nearPlane=63.3645, 
    farPlane=97.9302, cameraPosition=(-29.0842, 38.1134, -64.3496), 
    cameraUpVector=(-0.356526, 0.432076, 0.828372), cameraTarget=(6.19888e-06, 
    6.85453e-07, 0.500009))
session.viewports['Viewport: 1'].view.setValues(nearPlane=60.5689, 
    farPlane=100.726, cameraPosition=(5.30332, 52.4545, -60.5278), 
    cameraUpVector=(-0.0555988, 0.499176, 0.864715), cameraTarget=(3.15346e-06, 
    -7.56234e-07, 0.500009))
session.viewports['Viewport: 1'].view.setValues(nearPlane=69.4709, 
    farPlane=91.8238, cameraPosition=(19.5982, 14.3746, -76.3978), 
    cameraUpVector=(0.25229, 0.788265, 0.561238), cameraTarget=(1.95578e-06, 
    2.78652e-06, 0.50001))
session.viewports['Viewport: 1'].view.setValues(nearPlane=58.7792, 
    farPlane=102.515, cameraPosition=(-12.2133, 58.9926, -53.1162), 
    cameraUpVector=(0.0735321, 0.392704, 0.916721), cameraTarget=(2.33948e-06, 
    1.3113e-06, 0.50001))
session.viewports['Viewport: 1'].view.setValues(nearPlane=62.3172, 
    farPlane=98.9774, width=25.1267, height=14.3505, cameraPosition=(-11.6066, 
    65.4193, -46.1833), cameraTarget=(0.606745, 6.42672, 7.43295))
p = mdb.models['Model-1'].parts['Part-1']
c = p.cells
pickedCells = c.getSequenceFromMask(mask=('[#6 ]', ), )
v1, e, d1 = p.vertices, p.edges, p.datums
p.PartitionCellByPlaneThreePoints(point1=v1[40], point2=v1[33], 
    cells=pickedCells, point3=p.InterestingPoint(edge=e[52], rule=MIDDLE))
session.viewports['Viewport: 1'].view.setValues(nearPlane=62.2973, 
    farPlane=98.9973, width=30.3655, height=17.3425, cameraPosition=(-15.013, 
    65.3242, -45.512), cameraTarget=(-2.79966, 6.33158, 8.10421))
p = mdb.models['Model-1'].parts['Part-1']
c = p.cells
pickedCells = c.getSequenceFromMask(mask=('[#24 ]', ), )
v2, e1, d2 = p.vertices, p.edges, p.datums
p.PartitionCellByPlaneThreePoints(point1=v2[28], point2=v2[22], 
    cells=pickedCells, point3=p.InterestingPoint(edge=e1[27], rule=MIDDLE))
session.viewports['Viewport: 1'].view.setValues(nearPlane=62.296, 
    farPlane=98.9986, width=29.1753, height=16.6627, cameraPosition=(-16.3945, 
    59.2627, -51.8666), cameraTarget=(-4.18123, 0.270082, 1.74961))
p = mdb.models['Model-1'].parts['Part-1']
c = p.cells
pickedCells = c.getSequenceFromMask(mask=('[#a9 ]', ), )
v1, e, d1 = p.vertices, p.edges, p.datums
p.PartitionCellByPlaneThreePoints(cells=pickedCells, point1=p.InterestingPoint(
    edge=e[41], rule=MIDDLE), point2=p.InterestingPoint(edge=e[71], 
    rule=MIDDLE), point3=p.InterestingPoint(edge=e[68], rule=MIDDLE))
p = mdb.models['Model-1'].parts['Part-1']
c = p.cells
pickedCells = c.getSequenceFromMask(mask=('[#560 ]', ), )
v2, e1, d2 = p.vertices, p.edges, p.datums
p.PartitionCellByPlaneThreePoints(cells=pickedCells, point1=p.InterestingPoint(
    edge=e1[70], rule=MIDDLE), point2=p.InterestingPoint(edge=e1[91], 
    rule=MIDDLE), point3=p.InterestingPoint(edge=e1[88], rule=MIDDLE))
session.viewports['Viewport: 1'].view.setValues(nearPlane=59.0878, 
    farPlane=102.207, width=64.0576, height=36.5848, cameraPosition=(-20.4325, 
    60.1021, -50.0231), cameraTarget=(-8.21924, 1.10956, 3.59311))
session.viewports['Viewport: 1'].view.setValues(nearPlane=59.305, 
    farPlane=93.7022, cameraPosition=(52.3382, 35.7591, 44.043), 
    cameraUpVector=(-0.607435, 0.707484, -0.361233), cameraTarget=(-8.21925, 
    1.10957, 3.5931))
session.viewports['Viewport: 1'].view.setValues(nearPlane=66.8202, 
    farPlane=89.1517, cameraPosition=(32.5367, -8.69747, 71.2567), 
    cameraUpVector=(-0.30192, 0.952159, -0.0473082), cameraTarget=(-7.14675, 
    3.51746, 2.11913))
session.viewports['Viewport: 1'].view.setValues(nearPlane=67.2176, 
    farPlane=98.0574, cameraPosition=(-3.23964, 35.8899, 75.1172), 
    cameraUpVector=(0.0962343, 0.708235, -0.699387), cameraTarget=(-5.92585, 
    1.99587, 1.98739))
session.viewports['Viewport: 1'].view.setValues(nearPlane=65.9259, 
    farPlane=99.3491, width=78.3989, height=44.7754, cameraPosition=(1.25664, 
    35.8103, 74.989), cameraTarget=(-1.42957, 1.91624, 1.85914))
session.viewports['Viewport: 1'].view.setValues(nearPlane=66.9004, 
    farPlane=98.6581, cameraPosition=(-10.6109, 30.8791, 76.5912), 
    cameraUpVector=(0.0343775, 0.758265, -0.651039), cameraTarget=(-1.71538, 
    1.79748, 1.89773))
session.viewports['Viewport: 1'].view.setValues(nearPlane=68.3787, 
    farPlane=96.3528, cameraPosition=(6.09456, 23.175, 79.3245), 
    cameraUpVector=(-0.131179, 0.816216, -0.562658), cameraTarget=(-1.28514, 
    1.59907, 1.96812))
session.viewports['Viewport: 1'].view.setValues(nearPlane=65.5038, 
    farPlane=97.3922, cameraPosition=(64.2377, 24.8505, 44.0007), 
    cameraUpVector=(-0.281, 0.741923, -0.60876), cameraTarget=(-0.0720828, 
    1.63403, 1.23115))
session.viewports['Viewport: 1'].view.setValues(nearPlane=68.6084, 
    farPlane=95.1976, cameraPosition=(15.8926, 19.2402, 78.5237), 
    cameraUpVector=(-0.107979, 0.846918, -0.520644), cameraTarget=(-0.547332, 
    1.57888, 1.57052))
session.viewports['Viewport: 1'].view.setValues(nearPlane=70.6809, 
    farPlane=93.1249, width=59.0849, height=33.7448, cameraPosition=(20.9525, 
    19.5919, 77.362), cameraTarget=(4.51261, 1.93059, 0.408813))
p = mdb.models['Model-1'].parts['Part-1']
p.deleteFeatures(('Partition cell-1', 'Partition cell-2', 'Partition cell-2', 
    'Partition face-1', 'Partition cell-6', 'Partition cell-5', 
    'Partition cell-4', 'Partition cell-3', 'Partition face-1', 
    'Partition cell-6', 'Partition cell-5', 'Partition cell-4', 
    'Partition cell-3', ))
session.viewports['Viewport: 1'].view.setValues(nearPlane=68.8999, 
    farPlane=94.906, width=77.0571, height=44.0091, cameraPosition=(25.7871, 
    18.7988, 76.5112), cameraTarget=(9.34718, 1.13742, -0.441986))
p = mdb.models['Model-1'].parts['Part-1']
f, e, d1 = p.faces, p.edges, p.datums
t = p.MakeSketchTransform(sketchPlane=f[4], sketchUpEdge=e[7], 
    sketchPlaneSide=SIDE1, origin=(0.0, 0.0, 1.0))
s = mdb.models['Model-1'].ConstrainedSketch(name='__profile__', 
    sheetSize=80.64, gridSpacing=2.01, transform=t)
g, v, d, c = s.geometry, s.vertices, s.dimensions, s.constraints
s.setPrimaryObject(option=SUPERIMPOSE)
p = mdb.models['Model-1'].parts['Part-1']
p.projectReferencesOntoSketch(sketch=s, filter=COPLANAR_EDGES)
session.viewports['Viewport: 1'].view.setValues(nearPlane=73.4339, 
    farPlane=88.8113, width=72.5388, height=42.8572, cameraPosition=(2.54469, 
    -0.448262, 81.6226), cameraTarget=(2.54469, -0.448262, 1))
s.ConstructionLine(point1=(0.0, 6.5325), point2=(0.0, 5.02499999995343))
s.VerticalConstraint(entity=g[6], addUndoState=False)
s.ConstructionLine(point1=(-5.5275, 0.0), point2=(5.52750000004191, 0.0))
s.HorizontalConstraint(entity=g[7], addUndoState=False)
session.viewports['Viewport: 1'].view.setValues(nearPlane=77.2632, 
    farPlane=84.9819, width=28.2374, height=16.6831, cameraPosition=(0.64744, 
    11.543, 81.6226), cameraTarget=(0.64744, 11.543, 1))
s.ConstructionLine(point1=(-1.005, 19.095), point2=(-1.005, 18.592500000014))
s.VerticalConstraint(entity=g[8], addUndoState=False)
s.ConstructionLine(point1=(1.005, 18.5925), point2=(1.005, 17.0849999999814))
s.VerticalConstraint(entity=g[9], addUndoState=False)
session.viewports['Viewport: 1'].view.setValues(nearPlane=78.8981, 
    farPlane=83.347, width=14.4951, height=8.56396, cameraPosition=(1.22766, 
    0.172645, 81.6226), cameraTarget=(1.22766, 0.172645, 1))
s.FixedConstraint(entity=g[6])
s.FixedConstraint(entity=g[7])
s.ConstructionLine(point1=(-0.5025, 0.5025), point2=(-0.299213647842407, 
    0.5025))
s.HorizontalConstraint(entity=g[10], addUndoState=False)
s.ConstructionLine(point1=(-0.5025, -0.5025), point2=(-0.248317956924438, 
    -0.5025))
s.HorizontalConstraint(entity=g[11], addUndoState=False)
s.DistanceDimension(entity1=g[6], entity2=g[8], textPoint=(-0.767455339431763, 
    1.05004787445068), value=0.5)
s.DistanceDimension(entity1=g[6], entity2=g[9], textPoint=(0.800135850906372, 
    1.30510663986206), value=0.5)
s.DistanceDimension(entity1=g[7], entity2=g[10], textPoint=(-1.32730937004089, 
    0.305275440216064), value=0.5)
s.DistanceDimension(entity1=g[7], entity2=g[11], textPoint=(-1.35784697532654, 
    -0.429294347763062), value=0.5)
session.viewports['Viewport: 1'].view.setValues(nearPlane=78.5938, 
    farPlane=83.6514, width=23.6029, height=13.945, cameraPosition=(2.00076, 
    0.342318, 81.6226), cameraTarget=(2.00076, 0.342318, 1))
s.Line(point1=(-3.5175, 0.5), point2=(3.74114036560059, 0.5))
s.HorizontalConstraint(entity=g[12], addUndoState=False)
s.ParallelConstraint(entity1=g[10], entity2=g[12], addUndoState=False)
s.CoincidentConstraint(entity1=v[4], entity2=g[10], addUndoState=False)
s.Line(point1=(3.74114036560059, 0.5), point2=(3.7411403656024, -0.5))
s.VerticalConstraint(entity=g[13], addUndoState=False)
s.PerpendicularConstraint(entity1=g[12], entity2=g[13], addUndoState=False)
s.CoincidentConstraint(entity1=v[6], entity2=g[11], addUndoState=False)
s.Line(point1=(3.7411403656024, -0.5), point2=(-3.31983804702759, -0.5))
s.HorizontalConstraint(entity=g[14], addUndoState=False)
s.PerpendicularConstraint(entity1=g[13], entity2=g[14], addUndoState=False)
s.CoincidentConstraint(entity1=v[7], entity2=g[11], addUndoState=False)
s.Line(point1=(-3.31983804702759, -0.5), point2=(-3.5175, 0.5))
session.viewports['Viewport: 1'].view.setValues(nearPlane=71.5561, 
    farPlane=90.6891, width=87.8646, height=51.9119, cameraPosition=(4.05921, 
    0.342999, 81.6226), cameraTarget=(4.05921, 0.342999, 1))
s.Line(point1=(0.5, 21.8026123046875), point2=(-0.5, 21.8026123046875))
s.HorizontalConstraint(entity=g[16], addUndoState=False)
s.PerpendicularConstraint(entity1=g[9], entity2=g[16], addUndoState=False)
s.CoincidentConstraint(entity1=v[8], entity2=g[9], addUndoState=False)
s.CoincidentConstraint(entity1=v[9], entity2=g[8], addUndoState=False)
s.Line(point1=(-0.5, 21.8026123046875), point2=(-0.5, -22.7245407104492))
s.VerticalConstraint(entity=g[17], addUndoState=False)
s.PerpendicularConstraint(entity1=g[16], entity2=g[17], addUndoState=False)
s.CoincidentConstraint(entity1=v[10], entity2=g[8], addUndoState=False)
s.Line(point1=(-0.5, -22.7245407104492), point2=(0.5, -22.7245407104492))
s.HorizontalConstraint(entity=g[18], addUndoState=False)
s.PerpendicularConstraint(entity1=g[17], entity2=g[18], addUndoState=False)
s.CoincidentConstraint(entity1=v[11], entity2=g[9], addUndoState=False)
s.Line(point1=(0.5, -22.7245407104492), point2=(0.5, 21.8026123046875))
s.VerticalConstraint(entity=g[19], addUndoState=False)
s.PerpendicularConstraint(entity1=g[18], entity2=g[19], addUndoState=False)
session.viewports['Viewport: 1'].view.setValues(width=86.1073, height=50.8736, 
    cameraPosition=(4.10981, 0.747169, 81.6226), cameraTarget=(4.10981, 
    0.747169, 1))
p = mdb.models['Model-1'].parts['Part-1']
f = p.faces
pickedFaces = f.getSequenceFromMask(mask=('[#10 ]', ), )
e1, d2 = p.edges, p.datums
p.PartitionFaceBySketch(sketchUpEdge=e1[7], faces=pickedFaces, sketch=s)
s.unsetPrimaryObject()
del mdb.models['Model-1'].sketches['__profile__']
session.viewports['Viewport: 1'].view.setValues(nearPlane=66.2863, 
    farPlane=98.3442, cameraPosition=(27.4512, 30.4444, 72.4942), 
    cameraUpVector=(-0.135256, 0.75853, -0.637447), cameraTarget=(9.37269, 
    1.31596, -0.50357))
session.viewports['Viewport: 1'].view.setValues(nearPlane=66.2862, 
    farPlane=98.3442, cameraPosition=(27.4512, 30.4444, 72.4942), 
    cameraUpVector=(-0.23428, 0.75557, -0.611741), cameraTarget=(9.37269, 
    1.31596, -0.50357))
session.viewports['Viewport: 1'].view.setValues(nearPlane=66.4637, 
    farPlane=102.398, cameraPosition=(43.773, 36.6089, 63.3936), 
    cameraUpVector=(-0.36071, 0.70264, -0.613339), cameraTarget=(9.70342, 
    1.44087, -0.687977))
p = mdb.models['Model-1'].parts['Part-1']
c = p.cells
pickedCells = c.getSequenceFromMask(mask=('[#1 ]', ), )
e = p.edges
pickedEdges =(e[1], e[4], e[8], e[12], e[17], e[18], e[20], e[23])
p.PartitionCellBySweepEdge(sweepPath=e[27], cells=pickedCells, 
    edges=pickedEdges)
session.viewports['Viewport: 1'].view.setValues(nearPlane=72.3615, 
    farPlane=96.5002, width=18.3597, height=10.4856, cameraPosition=(37.5301, 
    36.8504, 66.5802), cameraTarget=(3.46053, 1.68234, 2.4986))
p = mdb.models['Model-1'].parts['Part-1']
c = p.cells
pickedCells = c.getSequenceFromMask(mask=('[#7 ]', ), )
e1 = p.edges
pickedEdges =(e1[25], e1[28], e1[30], e1[33], e1[34], e1[35], e1[36], e1[37])
p.PartitionCellBySweepEdge(sweepPath=e1[11], cells=pickedCells, 
    edges=pickedEdges)
session.viewports['Viewport: 1'].view.setValues(nearPlane=58.694, 
    farPlane=110.168, width=150.813, height=86.1326, cameraPosition=(37.6812, 
    34.9842, 67.524), cameraTarget=(3.61165, -0.18387, 3.44243))
mdb.saveAs(
    pathName='/home/ragnarok/Desktop/equivalence-Yeoh/cornea/abq-cae/probe-3D')
#: The model database has been saved to "/home/ragnarok/Desktop/equivalence-Yeoh/cornea/abq-cae/probe-3D.cae".
session.viewports['Viewport: 1'].view.setValues(session.views['Front'])
session.viewports['Viewport: 1'].partDisplay.setValues(sectionAssignments=ON, 
    engineeringFeatures=ON)
session.viewports['Viewport: 1'].partDisplay.geometryOptions.setValues(
    referenceRepresentation=OFF)
mdb.models['Model-1'].Material(name='Material-1')
mdb.models['Model-1'].materials['Material-1'].Hyperelastic(
    materialType=ISOTROPIC, testData=OFF, type=NEO_HOOKE, 
    volumetricResponse=VOLUMETRIC_DATA, table=((1.0, 0.01), ))
mdb.models['Model-1'].materials.changeKey(fromName='Material-1', toName='neo')
mdb.models['Model-1'].Material(name='yeoh')
mdb.models['Model-1'].materials['yeoh'].Hyperelastic(materialType=ISOTROPIC, 
    testData=OFF, type=YEOH, volumetricResponse=VOLUMETRIC_DATA, table=((1.0, 
    1.0, 11.0, 0.01, 0.01, 0.01), ))
mdb.models['Model-1'].HomogeneousSolidSection(name='cornea', material='neo', 
    thickness=None)
p = mdb.models['Model-1'].parts['Part-1']
c = p.cells
cells = c.getSequenceFromMask(mask=('[#1ff ]', ), )
region = p.Set(cells=cells, name='probe')
p = mdb.models['Model-1'].parts['Part-1']
p.SectionAssignment(region=region, sectionName='cornea', offset=0.0, 
    offsetType=MIDDLE_SURFACE, offsetField='', 
    thicknessAssignment=FROM_SECTION)
a = mdb.models['Model-1'].rootAssembly
session.viewports['Viewport: 1'].setValues(displayedObject=a)
session.viewports['Viewport: 1'].assemblyDisplay.setValues(
    optimizationTasks=OFF, geometricRestrictions=OFF, stopConditions=OFF)
a = mdb.models['Model-1'].rootAssembly
a.DatumCsysByDefault(CARTESIAN)
p = mdb.models['Model-1'].parts['Part-1']
a.Instance(name='Part-1-1', part=p, dependent=ON)
session.viewports['Viewport: 1'].assemblyDisplay.setValues(mesh=ON)
session.viewports['Viewport: 1'].assemblyDisplay.meshOptions.setValues(
    meshTechnique=ON)
session.viewports['Viewport: 1'].view.setValues(session.views['Front'])
p = mdb.models['Model-1'].parts['Part-1']
session.viewports['Viewport: 1'].setValues(displayedObject=p)
session.viewports['Viewport: 1'].partDisplay.setValues(sectionAssignments=OFF, 
    engineeringFeatures=OFF, mesh=ON)
session.viewports['Viewport: 1'].partDisplay.meshOptions.setValues(
    meshTechnique=ON)
p = mdb.models['Model-1'].parts['Part-1']
p.seedPart(size=1.0, deviationFactor=0.1, minSizeFactor=0.1)
elemType1 = mesh.ElemType(elemCode=C3D20H, elemLibrary=STANDARD)
elemType2 = mesh.ElemType(elemCode=C3D15, elemLibrary=STANDARD)
elemType3 = mesh.ElemType(elemCode=C3D10, elemLibrary=STANDARD)
p = mdb.models['Model-1'].parts['Part-1']
c = p.cells
cells = c.getSequenceFromMask(mask=('[#1ff ]', ), )
pickedRegions =(cells, )
p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2, 
    elemType3))
p = mdb.models['Model-1'].parts['Part-1']
p.generateMesh()
a = mdb.models['Model-1'].rootAssembly
a.regenerate()
a = mdb.models['Model-1'].rootAssembly
session.viewports['Viewport: 1'].setValues(displayedObject=a)
session.viewports['Viewport: 1'].assemblyDisplay.setValues(mesh=OFF)
session.viewports['Viewport: 1'].assemblyDisplay.meshOptions.setValues(
    meshTechnique=OFF)
session.viewports['Viewport: 1'].assemblyDisplay.setValues(mesh=ON)
a = mdb.models['Model-1'].rootAssembly
e1 = a.instances['Part-1-1'].elements
elements1 = e1.getSequenceFromMask(mask=('[#0:6 #1000 ]', ), )
a.Set(elements=elements1, name='CENTER')
#: The set 'CENTER' has been created (1 element).
session.viewports['Viewport: 1'].assemblyDisplay.setValues(mesh=OFF)
session.viewports['Viewport: 1'].view.setValues(nearPlane=71.4306, 
    farPlane=89.8641, cameraPosition=(26.5212, 1.95978, 76.6366), 
    cameraUpVector=(0.186566, 0.978296, -0.0901693))
session.viewports['Viewport: 1'].view.setValues(nearPlane=65.688, 
    farPlane=95.6067, cameraPosition=(36.9016, 24.033, 68.0625), 
    cameraUpVector=(-0.0189818, 0.945226, -0.325863), cameraTarget=(
    -8.19564e-08, 2.83122e-07, 0.5))
session.viewports['Viewport: 1'].view.setValues(nearPlane=63.0885, 
    farPlane=98.2064, cameraPosition=(34.6075, 34.8927, 64.4439), 
    cameraUpVector=(-0.124553, 0.897772, -0.422482), cameraTarget=(
    -1.12224e-07, 2.71946e-07, 0.5))
session.viewports['Viewport: 1'].assemblyDisplay.setValues(
    adaptiveMeshConstraints=ON)
mdb.models['Model-1'].StaticStep(name='Pull', previous='Initial', nlgeom=ON)
session.viewports['Viewport: 1'].assemblyDisplay.setValues(step='Pull')
session.viewports['Viewport: 1'].assemblyDisplay.setValues(loads=ON, bcs=ON, 
    predefinedFields=ON, connectors=ON, adaptiveMeshConstraints=OFF)
a = mdb.models['Model-1'].rootAssembly
f1 = a.instances['Part-1-1'].faces
faces1 = f1.getSequenceFromMask(mask=('[#c0 #20 ]', ), )
region = a.Set(faces=faces1, name='top-nodes')
mdb.models['Model-1'].DisplacementBC(name='pull', createStepName='Pull', 
    region=region, u1=0.0, u2=20.0, u3=0.0, ur1=0.0, ur2=0.0, ur3=0.0, 
    amplitude=UNSET, fixed=OFF, distributionType=UNIFORM, fieldName='', 
    localCsys=None)
session.viewports['Viewport: 1'].assemblyDisplay.setValues(step='Initial')
session.viewports['Viewport: 1'].view.setValues(nearPlane=59.9719, 
    farPlane=101.323, cameraPosition=(36.4318, -47.4786, 54.5602), 
    cameraUpVector=(0.326695, 0.808334, 0.489761), cameraTarget=(1.49012e-08, 
    1.78814e-07, 0.5))
a = mdb.models['Model-1'].rootAssembly
f1 = a.instances['Part-1-1'].faces
faces1 = f1.getSequenceFromMask(mask=('[#c00 #80 ]', ), )
region = a.Set(faces=faces1, name='bottom-nodes')
mdb.models['Model-1'].EncastreBC(name='encastre', createStepName='Initial', 
    region=region, localCsys=None)
session.viewports['Viewport: 1'].view.setValues(nearPlane=67.2329, 
    farPlane=94.0618, cameraPosition=(37.7622, -17.6589, 69.5375), 
    cameraUpVector=(0.172474, 0.972822, 0.154496), cameraTarget=(3.20375e-07, 
    4.24683e-06, 0.500002))
session.viewports['Viewport: 1'].view.setValues(farPlane=94.0618, 
    cameraPosition=(37.7622, -17.6589, 69.5375), cameraUpVector=(-0.231625, 
    0.904513, 0.358058), cameraTarget=(1.86265e-09, 4.00096e-06, 0.500002))
session.viewports['Viewport: 1'].view.setValues(nearPlane=67.0876, 
    farPlane=94.2071, cameraPosition=(66.3055, 15.3208, 43.7764), 
    cameraUpVector=(-0.171524, 0.981534, -0.0846831), cameraTarget=(
    -2.32458e-06, 1.3411e-06, 0.500004))
mdb.save()
#: The model database has been saved to "/home/ragnarok/Desktop/equivalence-Yeoh/cornea/abq-cae/probe-3D.cae".
session.viewports['Viewport: 1'].assemblyDisplay.setValues(loads=OFF, bcs=OFF, 
    predefinedFields=OFF, connectors=OFF)
session.viewports['Viewport: 1'].partDisplay.setValues(sectionAssignments=ON, 
    engineeringFeatures=ON, mesh=OFF)
session.viewports['Viewport: 1'].partDisplay.meshOptions.setValues(
    meshTechnique=OFF)
p = mdb.models['Model-1'].parts['Part-1']
session.viewports['Viewport: 1'].setValues(displayedObject=p)
a = mdb.models['Model-1'].rootAssembly
session.viewports['Viewport: 1'].setValues(displayedObject=a)
session.viewports['Viewport: 1'].assemblyDisplay.setValues(
    optimizationTasks=ON, geometricRestrictions=ON, stopConditions=ON)
session.viewports['Viewport: 1'].assemblyDisplay.setValues(
    optimizationTasks=OFF, geometricRestrictions=OFF, stopConditions=OFF)
mdb.Job(name='test-neo-3D', model='Model-1', description='', type=ANALYSIS, 
    atTime=None, waitMinutes=0, waitHours=0, queue=None, memory=90, 
    memoryUnits=PERCENTAGE, getMemoryFromAnalysis=True, 
    explicitPrecision=SINGLE, nodalOutputPrecision=SINGLE, echoPrint=OFF, 
    modelPrint=OFF, contactPrint=OFF, historyPrint=OFF, userSubroutine='', 
    scratch='', multiprocessingMode=DEFAULT, numCpus=1, numGPUs=0)
mdb.jobs['test-neo-3D'].writeInput(consistencyChecking=OFF)
#: The job input file has been written to "test-neo-3D.inp".
p = mdb.models['Model-1'].parts['Part-1']
session.viewports['Viewport: 1'].setValues(displayedObject=p)
mdb.models['Model-1'].sections['cornea'].setValues(material='yeoh', 
    thickness=None)
a = mdb.models['Model-1'].rootAssembly
session.viewports['Viewport: 1'].setValues(displayedObject=a)
mdb.Job(name='test-yeoh-3d', model='Model-1', description='', type=ANALYSIS, 
    atTime=None, waitMinutes=0, waitHours=0, queue=None, memory=90, 
    memoryUnits=PERCENTAGE, getMemoryFromAnalysis=True, 
    explicitPrecision=SINGLE, nodalOutputPrecision=SINGLE, echoPrint=OFF, 
    modelPrint=OFF, contactPrint=OFF, historyPrint=OFF, userSubroutine='', 
    scratch='', multiprocessingMode=DEFAULT, numCpus=1, numGPUs=0)
mdb.jobs.changeKey(fromName='test-yeoh-3d', toName='test-yeoh-3D')
mdb.jobs['test-yeoh-3D'].writeInput(consistencyChecking=OFF)
#: The job input file has been written to "test-yeoh-3D.inp".
o1 = session.openOdb(
    name='/home/ragnarok/Desktop/equivalence-Yeoh/cornea/abq-cae/x.odb')
session.viewports['Viewport: 1'].setValues(displayedObject=o1)
#: Model: /home/ragnarok/Desktop/equivalence-Yeoh/cornea/abq-cae/x.odb
#: Number of Assemblies:         1
#: Number of Assembly instances: 0
#: Number of Part instances:     1
#: Number of Meshes:             1
#: Number of Element Sets:       5
#: Number of Node Sets:          4
#: Number of Steps:              1
session.viewports['Viewport: 1'].odbDisplay.display.setValues(plotState=(
    CONTOURS_ON_DEF, ))
session.viewports['Viewport: 1'].view.setValues(nearPlane=49.294, 
    farPlane=100.454, width=63.257, height=36.1275, cameraPosition=(46.7388, 
    46.5829, 46.8637), cameraTarget=(0.177046, 0.0210762, 0.301871))
session.viewports['Viewport: 1'].view.setValues(session.views['Front'])
odb = session.odbs['/home/ragnarok/Desktop/equivalence-Yeoh/cornea/abq-cae/x.odb']
xyList = xyPlot.xyDataListFromField(odb=odb, outputPosition=ELEMENT_CENTROID, 
    variable=(('S', INTEGRATION_POINT, ((COMPONENT, 'S22'), )), ), 
    elementSets=('CENTER', ))
xyp = session.XYPlot('XYPlot-1')
chartName = xyp.charts.keys()[0]
chart = xyp.charts[chartName]
curveList = session.curveSet(xyData=xyList)
chart.setValues(curvesToPlot=curveList)
session.viewports['Viewport: 1'].setValues(displayedObject=xyp)
mdb.save()
#: The model database has been saved to "/home/ragnarok/Desktop/equivalence-Yeoh/cornea/abq-cae/probe-3D.cae".
