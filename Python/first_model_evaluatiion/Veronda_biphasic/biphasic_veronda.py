import numpy as np
import os
import matplotlib.pyplot as plt
import pickle
from my_functions import *


data = read_data_thief('Elsheikh2001_37dot5mmHgmin-1.txt')                         # data to optimize for [mm, mmHg]
data[:, 1] = data[:, 1]*0.000133322                                                # convert mmHg to [MPa]
data_2 = read_data_thief('elsheikh2007_quasi_static_age_1.txt')                         # data to optimize for [mm, mmHg]
data_2[:, 1] = data_2[:, 1]*0.000133322                                                    # convert mmHg to [MPa]
file_names = ['apical_rise.dat', 'apical_rise_HM.dat', 'apical_rise_visco.dat',
              'apical_rise_p_p.dat', 'apical_rise_HM_p_p.dat', 'apical_rise_visco_p_p.dat', 'k=0.00000085.dat']
fig1, ax1 = plt.subplots()
fig1.suptitle('Inflation test loading rate: 37.5 mmHg', fontsize=16)
parameters = [' biphasic', ' biphasic & H-M', ' biphasic & visco', ' biphasic & post. fluid pressure',
              ' biphasic & H-M & post. fluid pressure', ' biphasic & visco & post. fluid pressure', 'k = 0.00000085 mm⁴/(Ns)']
i = 0
for name in file_names:
    t, apical_rise_fem = load_output_dat_file(name)

    t = np.asarray(t)
    index_t_start = (np.abs(t - 100)).argmin()
    apical_rise_fem = np.asarray(apical_rise_fem)[:, 1]

    p_fem = np.zeros([1, len(t)])
    p_fem[0, 0:index_t_start] = 9.99918e-05/t[index_t_start]*t[0:index_t_start]
    p_fem[0, index_t_start:] = 0.0213316/(t[-1] - t[index_t_start])*(t[index_t_start:] - t[index_t_start]) + 9.99918e-05
    index_a = (np.abs(p_fem - 0.75*0.000133322)).argmin()
    a_rise_opt = apical_rise_fem - apical_rise_fem[index_a]
    ax1.plot(a_rise_opt[index_a:], p_fem[0, index_a:], label='simulation'+parameters[i])
    i += 1

ax1.scatter(data[::20, 0], data[::20, 1], label='experiment', marker='+')
ax1.scatter(data_2[::20, 0], data_2[::20, 1], label='experiment  quasi-static', marker='+', color='black')
ax1.set_xlabel('apical rise [mm]', Fontsize=12)
ax1.set_ylabel('pressure [MPa]', Fontsize=12)
ax1.legend()
plt.show()

