import numpy as np
import os
import matplotlib as plt
from copy import deepcopy
import itertools as itool
from my_functions import *


os.chdir("/mnt/BackUp/jeremy/FEBIO/sensitivity_analysis")  # get and create file in FEBio folder

nodes_mid_layer = np.zeros([4, 4])
nodes_mid_layer[0, :] = [2, -5.3173616e+00, -5.3173616e+00, 5.0000000e-02]
nodes_mid_layer[1, :] = [5, -5.3173616e+00,  5.3173616e+00,  5.0000000e-02]
nodes_mid_layer[2, :] = [8, 5.3173616e+00, -5.3173616e+00,  5.0000000e-02]
nodes_mid_layer[3, :] = [11, 5.3173616e+00,  5.3173616e+00,  5.0000000e-02]

E_epi = [0.02, 11, 16]
E_str = [0.01, 0.0175, 0.04]
nu_epi = [0.01, 0.075, 0.475]
nu_str = [0.01, 0.075, 0.475]
gamma_epi = [0.25, 0.75, 1.5]
gamma_str = [0.9, 1.25, 2]
tau_epi = [50, 150, 15000]
tau_str = [50, 150, 15000]
k_epi = [1e-6, 5e-6, 5e-5]
k_str = [5e-5, 8.5e-4, 5e-2]
ratio_thickens = [700/720, 500/550, 450/510]                        # total thickness constant just ratio in thi simulation
load = [0.000707355*0.9, 0.000707355, 0.000707355*1.1]
kk = list(itool.product([0, 1, 2], [0, 1, 2], [0, 1, 2], [0, 1, 2], [0, 1, 2], [0, 1, 2], [0, 1, 2], [0, 1, 2],
                        [0, 1, 2], [0, 1, 2], [0, 1, 2], [0, 1, 2]))
for i in range(3**11, 3**12):
        f = open("parameters.feb", "w+")
        f.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n")
        f.write("<febio_spec version=\"2.5\">\n")
        f.write("\t<Parameters>\n")
        f.write("\t\t<param name=\"young_epi\">"+str(E_epi[kk[i][0]])+"</param>\n")
        f.write("\t\t<param name=\"young_stroma\">"+str(E_str[kk[i][1]])+"</param>\n")
        f.write("\t\t<param name=\"poison_epi\">" + str(nu_epi[kk[i][2]]) + "</param>\n")
        f.write("\t\t<param name=\"poison_stroma\">" + str(nu_str[kk[i][3]]) + "</param>\n")
        f.write("\t\t<param name=\"gamma_epi\">" + str(gamma_epi[kk[i][4]]) + "</param>\n")
        f.write("\t\t<param name=\"gamma_stroma\">" + str(gamma_str[kk[i][5]]) + "</param>\n")
        f.write("\t\t<param name=\"tau_epi\">" + str(tau_epi[kk[i][6]]) + "</param>\n")
        f.write("\t\t<param name=\"tau_stroma\">" + str(tau_str[kk[i][7]]) + "</param>\n")
        f.write("\t\t<param name=\"permeability_epi\">" + str(k_epi[kk[i][8]]) + "</param>\n")
        f.write("\t\t<param name=\"permeability_stroma\">" + str(k_str[kk[i][9]]) + "</param>\n")
        f.write("\t\t<param name=\"load\">" + str(load[kk[i][11]]) + "</param>\n")
        f.write("\t</Parameters>\n")
        f.write("</febio_spec>")
        f.close()
        nodes_mid_layer[:, -1] = [ratio_thickens[kk[i][10]]*0.55, ratio_thickens[kk[i][10]]*0.55,
                                  ratio_thickens[kk[i][10]]*0.55, ratio_thickens[kk[i][10]]*0.55]
        write_febio_geometry_file('sensitivity_analysis.feb', nodes_mid_layer)
        if True:  # i != 1 or ii == 7:
            os.system("febio2 -i sensitivity_analysis.feb")
            os.system("mkdir /mnt/BackUp/jeremy/repository/keratometry/Python/first_model_evaluatiion/sensitivity_analysis/data/dir"+str(i))
            os.system("mv top_node.dat /mnt/BackUp/jeremy/repository/keratometry/Python/first_model_evaluatiion/sensitivity_analysis/data/dir"+str(i))
            os.system("mv middle_node.dat /mnt/BackUp/jeremy/repository/keratometry/Python/first_model_evaluatiion/sensitivity_analysis/data/dir" + str(i))
            os.system("mv top_el.dat /mnt/BackUp/jeremy/repository/keratometry/Python/first_model_evaluatiion/sensitivity_analysis/data/dir"+str(i))
            os.system("mv middle_el.dat /mnt/BackUp/jeremy/repository/keratometry/Python/first_model_evaluatiion/sensitivity_analysis/data/dir" + str(i))
            os.system("mkdir scratch/dir" + str(i))
            os.system("cp parameters.feb scratch/dir" + str(i) + "/parameters.feb")