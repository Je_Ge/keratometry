import numpy as np
import os
import matplotlib as plt
import pickle
from copy import deepcopy
from my_functions import *


ite_max = 10  # [-]
tol_error = 1e-3  # [mm]


main_path = '/mnt/BackUp/jeremy/FEBIO/OrthoK_veronda/20_degr_model'
path_2nd = '/mnt/BackUp/jeremy/repository/keratometry/Python/first_model_evaluatiion/OrthoK_Veronda'
os.chdir(main_path)  # get and create file in FEBio folder
os.system('cp geometry_init.feb geometry_opt.feb')

c_1 = 1e-4
c_2 = 19
k = 100
k_stroma = np.linspace(0.00085, 0.001, 1)
gamma_stroma = 7.5
tau_stroma = 5000

E_epi = 0.008                                                                              # Young's modulus [MPa]
nu_epi = 0.075                                                                             # Poison ratio [-]
k_epi = np.logspace(-6, -3, 1)
gamma_epi = 0
tau_epi = 0

time_prestretch = tau_stroma*5
time_initiating_contact = time_prestretch + 10
time_load_end = time_initiating_contact + 28790
time_unload_end = time_load_end + 57600
parameter_name = ['c_1', 'c_2', 'k', 'k_stroma', 'gamma_stroma', 'tau_stroma', 'E_epi', 'nu_epi', 'k_epi', 'gamma_epi',
                  'tau_epi', 'time_prestretch', 'time_initiating_contact', 'time_load_end', 'time_unload_end']
time_1 = [0, time_prestretch/5, time_prestretch]
magnitude_1 = [0, 1, 1]
time_2 = [0, time_prestretch/20, time_prestretch/10, time_prestretch/5, time_prestretch]
magnitude_2 = [0.5, tau_stroma/10, tau_stroma/20, tau_stroma/10, tau_stroma/1.5]
time_3 = [time_prestretch, time_initiating_contact]
magnitude_3 = [-0.5, 1]
time_4 = [time_initiating_contact, time_initiating_contact+120]
magnitude_4 = [0.15, 1]
time_5 = [time_load_end, time_load_end+90, time_load_end+90.5]
magnitude_5 = [-0.04, 0.01, 1]
time_6 = [time_prestretch, time_prestretch+11, time_prestretch+15000, time_load_end]
magnitude_6 = [0.1, 0.5, 550, 450]
time_7 = [time_load_end, time_load_end+10, time_load_end+500, time_load_end+2500, time_unload_end]
magnitude_7 = [0.1, 2, 150, 1000, 750]
for j in range(len(k_epi)):
    for jj in range(len(k_stroma)):

        parameter = [c_1, c_2, k, k_stroma[jj], gamma_stroma, tau_stroma, E_epi, nu_epi, k_epi[j], gamma_epi, tau_epi,
                     10*time_prestretch, 10*time_initiating_contact, 10*time_load_end, 10*time_unload_end]

        write_parameters(parameter, parameter_name, path=main_path)
        write_loadcurve(time_1, magnitude_1, 'pre_stretch_load_curve.feb', 1, path=main_path)
        write_loadcurve(time_2, magnitude_2, 'pre_stretch_must_point_curve.feb', 2, path=main_path)
        write_loadcurve(time_3, magnitude_3, 'initiating_contact_load_curve.feb', 3, path=main_path)
        write_loadcurve(time_4, magnitude_4, 'load_curve.feb', 4, path=main_path)
        write_loadcurve(time_5, magnitude_5, 'unload_curve.feb', 5, path=main_path)
        write_loadcurve(time_6, magnitude_6, 'must_point_curve_1.feb', 6, path=main_path)
        write_loadcurve(time_7, magnitude_7, 'must_point_curve_2.feb', 7, path=main_path)

        pre_stretch(ite_max, tol_error, path=main_path)
        path_temp = os.path.join(main_path, 'temp_' + str(j) + str(jj))
        os.system('mkdir ' + path_temp)
        os.system('cp geometry_opt.feb ' + path_temp + '/geometry_opt.feb')
        os.system('febio2 -i 1_day_with_prestretch.feb')
        path_temp = os.path.join(path_2nd, 'dir_' + str(j) + str(jj))
        os.system('mkdir ' + path_temp)
        os.system('mv anterior_surface.dat ' + path_temp)
        os.system('mv anterior_surface_displacement.dat ' + path_temp)
