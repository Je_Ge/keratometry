import numpy as np
import os
import matplotlib as plt
import pickle
from copy import deepcopy
from my_functions import *


fig1, ax1 = plt.subplots()
fig1.suptitle('optimal parameters of Inflation test loading rate: 37.5 mmHg', fontsize=20)
fig2, ax2 = plt.subplots()
fig2.suptitle('Inflation test loading rate: 37.5 mmHg', fontsize=20)
fig3, ax3 = plt.subplots()
fig3.suptitle('summed squared error of Inflation test loading rate: 37.5 mmHg', fontsize=20)

E = 0.017
# gamma = np.linspace(3, 5, 10)
# tau = np.linspace(2000, 2800, 10)

gamma = np.linspace(5, 7.5, 10)
tau = np.linspace(500, 5000, 10)

error_fem = pickle.load(open("error_fem.pkl", "rb"))
error_fem_2 = deepcopy(error_fem)
p_fem = pickle.load(open("p_fem.pkl", "rb"))
apical_rise_fem = pickle.load(open("apical_rise_fem.pkl", "rb"))
E_fem = pickle.load(open("E_fem.pkl", "rb"))
index = np.unravel_index(np.argmin(error_fem, axis=None), error_fem.shape)
data = read_data_thief('Whitford2018_37dot5_mmHg_per_min.txt')                         # data to optimize for [mm, mmHg]
data[:, 1] = data[:, 1]*0.000133322                                                    # convert mmHg to [MPa]

ax1.set_xlabel('$\gamma$ [MPa]', Fontsize=12)
ax1.set_ylabel('squared error', Fontsize=12)
for i in range(len(gamma)):
    for ii in range(len(tau)):
        index_a = (np.abs(p_fem[str(i) + str(ii)] - 0.75*0.000133322)).argmin()
        a_rise_temp = apical_rise_fem[str(i) + str(ii)] - apical_rise_fem[str(i) + str(ii)][index_a]
        p_paper = np.interp(a_rise_temp, data[:, 0], data[:, 1])
        error_fem_2[i, ii] = np.sum((p_paper - p_fem[str(i) + str(ii)]) ** 2)
        ax2.plot(a_rise_temp, p_fem[str(i) + str(ii)], label='simulation'+str(i)+str(ii))

for i in range(len(tau)):
    ax3.plot(gamma, error_fem_2[:, i], label=str(tau[i]) + ' [s]', marker='+')
ax3.legend()
ax3.set_xlabel('$\gamma$ [MPa]', Fontsize=12)
ax3.set_ylabel('squared error', Fontsize=12)

index = np.unravel_index(np.argmin(error_fem, axis=None), error_fem.shape)
index_2 = np.unravel_index(np.argmin(error_fem_2, axis=None), error_fem_2.shape)

ax2.scatter(data[::20, 0], data[::20, 1], label='experiment', marker="+")
ax2.legend(fontsize=5)
ax2.set_xlabel('apical rise [mm]', Fontsize=12)
ax2.set_ylabel('pressure [MPa]', Fontsize=12)

index_a = (np.abs(p_fem[str(index[0]) + str(index[1])] - 0.75*0.000133322)).argmin()
index_a_2 = (np.abs(p_fem[str(index_2[0]) + str(index_2[1])] - 0.75*0.000133322)).argmin()
a_rise_opt = apical_rise_fem[str(index[0]) + str(index[1])] - apical_rise_fem[str(index[0]) + str(index[1])][index_a]
a_rise_opt_2 = apical_rise_fem[str(index_2[0]) + str(index_2[1])] - apical_rise_fem[str(index_2[0]) + str(index_2[1])][index_a_2]
ax1.plot(a_rise_opt, p_fem[str(index[0]) + str(index[1])], label='simulation with E cost; $\gamma$ = ' + str(np.round(gamma[index[0]], 6))
         + ' MPa' + ' $tau$ = ' + str(np.round(tau[index[1]], 3)) + ' s')
ax1.plot(a_rise_opt_2, p_fem[str(index_2[0]) + str(index_2[1])], label='simulation; $\gamma$ = ' + str(np.round(gamma[index_2[0]], 6))
         + ' MPa' + ' $tau$ = ' + str(np.round(tau[index_2[1]], 3)) + ' s')
ax1.scatter(data[::20, 0], data[::20, 1], label='experiment', marker="+")
ax1.set_xlabel('apical rise [mm]', Fontsize=12)
ax1.set_ylabel('pressure [MPa]', Fontsize=12)
ax1.legend()
plt.show()
print(gamma[index[0]], tau[index[1]], E_fem[str(index[0]) + str(index[1])])
