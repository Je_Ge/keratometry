import numpy as np
import os
import matplotlib as plt
import pickle
from copy import deepcopy
from my_functions import *


ite_max = 10  # [-]
tol_error = 1e-3  # [mm]


main_path = '/mnt/BackUp/jeremy/FEBIO/Veronda_biphasic_visco'
path_2nd = '/mnt/BackUp/jeremy/repository/keratometry/Python/first_model_evaluatiion/Veronda_biphasic_visco'
os.chdir(main_path)  # get and create file in FEBio folder
os.system('cp geometry_init.feb geometry_opt.feb')
data_3dot75 = read_data_thief('Whitford2018_3dot75_mmHg_per_min.txt', path=path_2nd)           # data to optimize for [mm, mmHg]
# data_37dot5 = read_data_thief('Whitford2018_37dot5_mmHg_per_min.txt', path=path_2nd)           # data to optimize for [mm, mmHg]

c_1 = 5e-5  # 1e-4
c_2 = 16  # 19
gamma = np.linspace(5.5, 9, 1)  # 6.388889
tau = np.linspace(5000, 8000, 1)  # 500
magnitude = [0, 9.99918e-05, 0.0001008250555, 0.0053329]                    # [0, 9.99918e-05, 0.00010832444, 0.0053329]

E = 0.017                                                                              # Young's modulus [MPa]
nu = 0.075                                                                             # Poison ratio [-]
k = 100  # E/(3*(1-2*nu))
p_fem = {}
t_end = {}
E_fem = {}
apical_rise_fem = {}
error_fem = np.ones([len(gamma), len(tau)])
for j in range(len(gamma)):
    for jj in range(len(tau)):
        time_inf = 12                                                                   # tau[jj]*5
        time_end = time_inf + 628
        time = [0, time_inf, time_inf+0.1, time_end]                                    # [0, time_inf/5, time_inf, time_inf+0.1, time_end]
        magnitude_2 = [0.1, time_inf/3, 0.2, 7]                                      # [0.1, time_inf/10 ,time_inf/3, 0.5, 10]
        write_parameters([c_1, c_2, k, gamma[j], tau[jj], 10*time_inf, 10*time_end], ['c_1', 'c_2', 'k', 'gamma', 'tau', 'time_inf', 'time_end'], path=main_path)
        write_loadcurve(time, magnitude, 'load_curve.feb', 1, path=main_path)
        write_loadcurve(time, magnitude_2, 't_max_curve.feb', 2, path=main_path)
        pre_stretch(ite_max, tol_error, path=main_path)
        path_temp = os.path.join(main_path, 'temp_' + str(j) + str(jj))
        os.system('mkdir ' + path_temp)
        os.system('cp geometry_opt.feb ' + path_temp + '/geometry_opt.feb')
        os.system('febio2 -i Veronda_3dot75.feb')
        os.system('mv apical_rise.dat ' + path_temp)
        os.system('mv stress_strain.dat ' + path_temp)
        t, u = load_output_dat_file('apical_rise.dat', path_temp)
        t_2, temp = load_output_dat_file('stress_strain.dat', path_temp)
        t = np.asarray(t)
        t_end[str(j) + str(jj)] = t[-1]
        u = np.asarray(u)[:, 1]
        apical_rise_fem[str(j) + str(jj)] = u
        t_2 = np.asarray(t_2)
        temp = np.asarray(temp)
        E_fem[str(j) + str(jj)] = temp[0, 1]/temp[0, 2]
        index_t_start = (np.abs(t - time_inf)).argmin()
        index_t_pre = (np.abs(t - time_inf/5)).argmin()
        p_fem[str(j) + str(jj)] = np.interp(t, time, magnitude)
        index_a = (np.abs(p_fem[str(j) + str(jj)] - 0.75*0.000133322)).argmin()
        a_rise_temp = u - u[index_a]
        p_paper = np.interp(a_rise_temp, data_3dot75[:, 0], data_3dot75[:, 1]*0.000133322 )
        error_fem[j, jj] = np.sum((p_paper - p_fem[str(j) + str(jj)]) ** 2)  # + (E - E_fem[str(j) + str(jj)])**2
index = np.unravel_index(np.argmin(error_fem, axis=None), error_fem.shape)
print(gamma[index[0]], tau[index[1]], E_fem[str(index[0]) + str(index[1])])
print(t_end)
f = open("apical_rise_fem.pkl", "wb")
pickle.dump(apical_rise_fem, f)
f.close()
f = open("p_fem.pkl", "wb")
pickle.dump(p_fem, f)
f.close()
f = open("E_fem.pkl", "wb")
pickle.dump(E_fem, f)
f.close()
f = open("error_fem.pkl", "wb")
pickle.dump(error_fem, f)
f.close()
fig1, ax1 = plt.subplots()

a_rise_opt = apical_rise_fem[str(index[0]) + str(index[1])]
index_a = (np.abs(p_fem[str(index[0]) + str(index[1])] - 0.75*0.000133322)).argmin()
ax1.plot(a_rise_opt - a_rise_opt[index_a], p_fem[str(index[0]) + str(index[1])]/0.000133322 , label='simulation')
ax1.scatter(data_3dot75[::20, 0], data_3dot75[::20, 1], label='experiment', marker='+')
ax1.legend()
ax1.set_xlabel('apical rise [mm]', Fontsize=12)
ax1.set_ylabel('pressure [MPa]', Fontsize=12)
plt.show()
