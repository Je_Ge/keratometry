import numpy as np
import os
import matplotlib as plt
import pickle
from copy import deepcopy
from my_functions import *


ite_max = 10  # [-]
tol_error = 1e-3  # [mm]


main_path = '/mnt/BackUp/jeremy/FEBIO/Veronda_biphasic_visco'
path_2nd = '/mnt/BackUp/jeremy/repository/keratometry/Python/first_model_evaluatiion/Veronda_biphasic_visco'
os.chdir(main_path)                                                                            # move into FEBio folder
os.system('cp geometry_init.feb geometry_opt.feb')
data_3dot75 = read_data_thief('Whitford2018_3dot75_mmHg_per_min.txt', path=path_2nd)           # data to optimize for [mm, mmHg]
data_37dot5 = read_data_thief('Whitford2018_37dot5_mmHg_per_min.txt', path=path_2nd)           # data to optimize for [mm, mmHg]
data = {'3dot75': data_3dot75, '37dot5': data_37dot5}

c_1 = np.logspace(-4.6, -3.7, 3)
c_2 = np.linspace(14.5, 19, 3)
gamma = np.linspace(1.3, 9, 3)  # 6.388889
tau = np.linspace(500, 15000, 3)  # 500

time_prestretch = 12                                            # 3.75 mmHG/min to prestretch to 0.75 mmHg (Assumption)
time_end = {'3dot75': time_prestretch + 628,
            '37dot5': time_prestretch + 62.8}                   # loading rate 1: 3.75; loading rate 1: 37.5 [mmHg/min]
magnitude_1 = {'3dot75': [0, 9.99918e-05, 0.0053329],
               '37dot5': [0, 9.99918e-05, 0.0053329]}
magnitude_2 = [0.1, 5]


E = 0.017                                                                              # Young's modulus [MPa]
nu = 0.075                                                                             # Poison ratio [-]
k = 100  # E/(3*(1-2*nu))
p_fem = {}
t_end = {}
E_fem = {}
apical_rise_fem = {}
p_paper = {}
error_fem = np.ones([len(c_1), len(c_2), len(gamma), len(tau)])
for i in range(len(c_1)):
    for ii in range(len(c_2)):
        for j in range(len(gamma)):
            for jj in range(len(tau)):
                write_parameters([c_1[i], c_2[ii], k, gamma[j], tau[jj], 10 * time_prestretch],
                                 ['c_1', 'c_2', 'k', 'gamma', 'tau', 'time_prestretch'], 'parameters_prestretch.feb', path=main_path)
                pre_stretch(ite_max, tol_error, path=main_path)
                path_temp = os.path.join(main_path, 'temp_' + str(i) + '_' + str(ii) + '_' + str(j) + '_' + str(jj))
                os.system('mkdir ' + path_temp)
                os.system('cp geometry_opt.feb ' + path_temp + '/geometry_opt.feb')

                for load_rate in magnitude_1:
                    time = [0, time_prestretch, time_end[load_rate]]
                    write_parameters([c_1[i], c_2[ii], k, gamma[j], tau[jj], 10*time_end[load_rate]],
                                     ['c_1', 'c_2', 'k', 'gamma', 'tau', 'time_end'], 'parameters_'+ load_rate + '.feb',path=main_path)
                    write_loadcurve(time, magnitude_1[load_rate], 'load_curve.feb', 1, path=main_path)
                    write_loadcurve(time, magnitude_2, 't_max_curve.feb', 2, path=main_path)

                    os.system('febio2 -i Veronda_' + load_rate + '.feb')
                    os.system('mv parameters_'+ load_rate + '.feb ' + path_temp)
                    os.system('mv apical_rise_' + load_rate + '.dat ' + path_temp)
                    t, u = load_output_dat_file('apical_rise_' + load_rate + '.dat', path_temp)

                    t = np.asarray(t)
                    t_end[str(i) + str(ii) + str(j) + str(jj) + load_rate] = t[-1]
                    u = np.asarray(u)[:, 1]
                    apical_rise_fem[str(i) + str(ii) + str(j) + str(jj) + load_rate] = u

                    p_fem[str(i) + str(ii) + str(j) + str(jj) + load_rate] = np.interp(t, time, magnitude_1[load_rate])
                    index_a = (np.abs(p_fem[str(i) + str(ii) + str(j) + str(jj) + load_rate]
                                      - 0.75*0.000133322)).argmin()
                    a_rise_temp = u - u[index_a]
                    p_paper[load_rate] = np.interp(a_rise_temp, data[load_rate][:, 0], data[load_rate][:, 1]*0.000133322)

                os.chdir(main_path)
                os.system('febio2 -i Veronda_static.feb')
                os.system('mv stress_strain.dat ' + path_temp)
                t_2, temp = load_output_dat_file('stress_strain.dat', path_temp)
                t_2 = np.asarray(t_2)
                temp = np.asarray(temp)
                E_fem[str(i) + str(ii) + str(j) + str(jj)] = temp[0, 1] / temp[0, 2]
                error_fem[i, ii, j, jj] = np.sum((p_paper['3dot75'] - p_fem[str(i) + str(ii) + str(j) + str(jj) + '3dot75'])**2)\
                                   + np.sum((p_paper['37dot5'] - p_fem[str(i) + str(ii) + str(j) + str(jj) + '37dot5'])**2)\
                                   + (E - E_fem[str(i) + str(ii) + str(j) + str(jj)])**2
index = np.unravel_index(np.argmin(error_fem, axis=None), error_fem.shape)
print(c_1[index[0]], c_2[index[1]], gamma[index[2]], tau[index[3]],
      E_fem[str(index[0]) + str(index[1]) + str(index[2]) + str(index[3])])
print(t_end)
f = open("apical_rise_fem.pkl", "wb")
pickle.dump(apical_rise_fem, f)
f.close()
f = open("p_fem.pkl", "wb")
pickle.dump(p_fem, f)
f.close()
f = open("E_fem.pkl", "wb")
pickle.dump(E_fem, f)
f.close()
f = open("error_fem.pkl", "wb")
pickle.dump(error_fem, f)
f.close()
fig1, ax1 = plt.subplots()
for load_rate in magnitude_1:
    a_rise_opt = apical_rise_fem[str(index[0]) + str(index[1]) + str(index[2]) + str(index[3]) + load_rate]
    index_a = (np.abs(p_fem[str(index[0]) + str(index[1]) + str(index[2]) + str(index[3]) + load_rate] - 0.75*0.000133322)).argmin()
    ax1.plot(a_rise_opt - a_rise_opt[index_a],
             p_fem[str(index[0]) + str(index[1]) + str(index[2]) + str(index[3]) + load_rate]/0.000133322,
             label='simulation ' + load_rate + '$\dfrac{mmHg}{min}$')

ax1.scatter(data_3dot75[::20, 0], data_3dot75[::20, 1], label='experiment 3.75 $\dfrac{mmHg}{min}$', marker='+')
ax1.scatter(data_37dot5[::20, 0], data_37dot5[::20, 1], label='experiment 37.5 $\dfrac{mmHg}{min}$', marker='+')
ax1.legend()
ax1.set_xlabel('apical rise [mm]', Fontsize=12)
ax1.set_ylabel('pressure [MPa]', Fontsize=12)
plt.show()
