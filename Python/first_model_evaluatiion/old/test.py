import numpy as np
from my_functions import *
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt


R_x = 7.6
R_y = 7.6
Q_x = -0
Q_y = -0
mz = 0.5
numb = 100
numb = 100
x, y = np.meshgrid(np.linspace(-3.5, 3.5, num=numb), np.linspace(-3.5, 3.5, num=numb), indexing='ij')
z = mz + (x**2/R_x + y**2/R_y)/(1 + np.sqrt(1 - ((1+Q_x)*x**2)/R_x**2 - ((1+Q_y)*y**2)/R_y**2)) # + 1e-7*np.random.rand(numb, numb)
pos = np.array(np.zeros([numb**2, 3]))
pos[:, 0:1] = np.resize(x, [numb**2, 1])
pos[:, 1:2] = np.resize(y, [numb**2, 1])
pos[:, 2:3] = np.resize(z, [numb**2, 1])
## fiting of the explixti form from above
# init = np.zeros([5, 1]) # np.array([R_x, R_y, Q_x, Q_y, mz])
# res = optimize.minimize(f2_biconic_model, init, np.array(pos), method='CG', options={'xtol': 1e-8})
# print(res.x)

# formula of paper
# r = biconic_fitting(np.array(pos))

# formula of paper extended with the midpoint mz
r = nm_biconic_fit(pos)
print('optimized parameter: ' + str([r[0][0], r[1][0], r[2][0], r[3][0], r[4][0], r[5]]))
print('actual solution: ' + str([R_x, R_y, 0, Q_x, Q_y, mz]))

# sphere fit
r = sphere_fit(pos)
print(r)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(x, y, z)
ax.set_xlim3d(-6, 6)
ax.set_ylim3d(-6, 6)
ax.set_zlim3d(-6, 6)
plt.show()