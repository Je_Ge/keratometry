import numpy as np
import os
import matplotlib as plt
import pickle
from copy import deepcopy
from my_functions import *


ite_max = 10  # [-]
tol_error = 1e-3  # [mm]


main_path = '/mnt/BackUp/jeremy/FEBIO/Veronda_eval'
path_2nd = '/mnt/BackUp/jeremy/repository/keratometry/Python/first_model_evaluatiion/Veronda_eval'
os.chdir(main_path)  # get and create file in FEBio folder
os.system('cp geometry_init.feb geometry_opt.feb')
data = read_data_thief('Whitford2018_3dot75_mmHg_per_min.txt', path=path_2nd)           # data to optimize for [mm, mmHg] # 'elsheikh2007_quasi_static_age_1.txt'
data[:, 1] = data[:, 1]*0.000133322                                                    # convert mmHg to [MPa]

c_1 = np.linspace(6e-4, 1e-4, 5)
c_2 = np.linspace(17, 19, 5)
E = 0.017                                                                              # Young's modulus [MPa]
nu = 0.075                                                                            # Poison ratio [-]
k = 100  # E/(3*(1-2*nu))
p_fem = {}
t_end = {}
E_fem = {}
apical_rise_fem = {}
error_fem = np.ones([len(c_1), len(c_2)])
for j in range(len(c_1)):
    for jj in range(len(c_2)):
        write_parameters([c_1[j], c_2[jj], k], ['c_1', 'c_2', 'k'], path=main_path)
        pre_stretch(ite_max, tol_error, path=main_path)
        path_temp = os.path.join(main_path, 'temp_' + str(j) + str(jj))
        os.system('mkdir ' + path_temp)
        os.system('cp geometry_opt.feb ' + path_temp + '/geometry_opt.feb')
        os.system('febio2 -i Veronda.feb')
        os.system('mv apical_rise.dat ' + path_temp)
        os.system('mv stress_strain.dat ' + path_temp)
        t, u = load_output_dat_file('apical_rise.dat', path_temp)
        t_2, temp = load_output_dat_file('stress_strain.dat', path_temp)
        t = np.asarray(t)
        t_end[str(j) + str(jj)] = t[-1]
        u = np.asarray(u)[:, 1]
        apical_rise_fem[str(j) + str(jj)] = u
        t_2 = np.asarray(t_2)
        temp = np.asarray(temp)
        E_fem[str(j) + str(jj)] = temp[0, 1]/temp[0, 2]
        p_fem[str(j) + str(jj)] = 0.0213316/2560*t                                 # convert load curve time to pressure

        index_a = (np.abs(p_fem[str(j) + str(jj)] - 0.75*0.000133322)).argmin()
        a_rise_temp = u - u[index_a]
        p_paper = np.interp(a_rise_temp, data[:, 0], data[:, 1])
        error_fem[j, jj] = np.sum((p_paper - p_fem[str(j) + str(jj)]) ** 2)  # + (E - E_fem[str(j) + str(jj)])**2
index = np.unravel_index(np.argmin(error_fem, axis=None), error_fem.shape)
print(c_1[index[0]], c_2[index[1]], E_fem[str(index[0]) + str(index[1])])
print(t_end)
f = open("apical_rise_fem.pkl", "wb")
pickle.dump(apical_rise_fem, f)
f.close()
f = open("p_fem.pkl", "wb")
pickle.dump(p_fem, f)
f.close()
f = open("E_fem.pkl", "wb")
pickle.dump(E_fem, f)
f.close()
f = open("error_fem.pkl", "wb")
pickle.dump(error_fem, f)
f.close()
fig1, ax1 = plt.subplots()
fig2, ax2 = plt.subplots()
#ax1.scatter(c_1, error_fem[:, 0], label=str(c_2[0]), marker='+')
#ax1.scatter(c_1, error_fem[:, 2], label=str(c_2[2]), marker='+')
#ax1.scatter(c_1, error_fem[:, 4], label=str(c_2[4]), marker='+')
#ax1.scatter(c_1, error_fem[:, 6], label=str(c_2[6]), marker='+')
#ax1.scatter(c_1, error_fem[:, 9], label=str(c_2[9]), marker='+')
ax1.legend()
ax1.set_xlabel('c_1 [MPa]', Fontsize=12)
ax1.set_ylabel('squared error', Fontsize=12)

a_rise_opt = apical_rise_fem[str(index[0]) + str(index[1])]
index_a = (np.abs(p_fem[str(index[0]) + str(index[1])] - 0.75*0.000133322)).argmin()
ax2.plot(a_rise_opt - a_rise_opt[index_a], p_fem[str(index[0]) + str(index[1])], label='simulation')

# a_rise_opt = apical_rise_fem[str(0) + str(0)]
# index_a = (np.abs(p_fem[str(0) + str(0)] - 0.75*0.000133322)).argmin()
# ax2.plot(a_rise_opt - a_rise_opt[index_a], p_fem[str(0) + str(0)], label='simulation')
# ax2.plot(apical_rise_fem[str(0) + str(1)], p_fem[str(0) + str(1)], label='simulation_2')
# ax2.plot(apical_rise_fem[str(0) + str(2)], p_fem[str(0) + str(2)], label='simulation_3')
# ax2.plot(apical_rise_fem[str(0) + str(3)], p_fem[str(0) + str(3)], label='simulation_4')
ax2.scatter(data[::20, 0], data[::20, 1], label='experiment', marker='+')
ax2.legend()
ax2.set_xlabel('apical rise [mm]', Fontsize=12)
ax2.set_ylabel('pressure [MPa]', Fontsize=12)
plt.show()
