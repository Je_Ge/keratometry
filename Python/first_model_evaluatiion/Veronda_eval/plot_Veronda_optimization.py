import numpy as np
import os
import matplotlib as plt
import pickle
from copy import deepcopy
from my_functions import *


fig1, ax1 = plt.subplots()
fig1.suptitle('optimal parameters of Inflation test loading rate: 3.75 mmHg', Fontsize=20)
fig2, ax2 = plt.subplots()
fig2.suptitle('Inflation test loading rate: 3.75 mmHg', Fontsize=20)
fig3, ax3 = plt.subplots()
fig3.suptitle('summed squared error of Inflation test loading rate: 3.75 mmHg', Fontsize=20)

E = 0.017
c_1 = 2*np.logspace(-5, -4, 20)[8:18]
c_2 = np.linspace(38, 44, 10)

# c_1 = np.linspace(1.5e-4, 7e-4, 6)
# c_2 = np.linspace(39.5, 40.5, 3)

error_fem = pickle.load(open("error_fem.pkl", "rb"))
error_fem_2 = deepcopy(error_fem)
p_fem = pickle.load(open("p_fem.pkl", "rb"))
apical_rise_fem = pickle.load(open("apical_rise_fem.pkl", "rb"))
E_fem = pickle.load(open("E_fem.pkl", "rb"))
index = np.unravel_index(np.argmin(error_fem, axis=None), error_fem.shape)
data = read_data_thief('elsheikh2007_quasi_static_age_1.txt')                         # data to optimize for [mm, mmHg]
data[:, 1] = data[:, 1]*0.000133322                                                    # convert mmHg to [MPa]

ax1.set_xlabel('c_1 [MPa]', Fontsize=12)
ax1.set_ylabel('squared error', Fontsize=12)
for i in range(len(c_1)):
    for ii in range(len(c_2)):
        index_a = (np.abs(p_fem[str(i) + str(ii)] - 0.75*0.000133322)).argmin()
        a_rise_temp = apical_rise_fem[str(i) + str(ii)] - apical_rise_fem[str(i) + str(ii)][index_a]
        p_paper = np.interp(a_rise_temp, data[:, 0], data[:, 1])
        error_fem_2[i, ii] = np.sum((p_paper - p_fem[str(i) + str(ii)]) ** 2)
        ax2.plot(a_rise_temp, p_fem[str(i) + str(ii)], label='simulation'+str(i)+str(ii))

for i in range(len(c_2)):
    ax3.plot(c_1, error_fem_2[:, i], label=str(c_2[i]), marker='+')
ax3.legend()
ax3.set_xlabel('c_1 [MPa]', Fontsize=12)
ax3.set_ylabel('squared error', Fontsize=12)

index = np.unravel_index(np.argmin(error_fem, axis=None), error_fem.shape)
index_2 = np.unravel_index(np.argmin(error_fem_2, axis=None), error_fem_2.shape)

ax2.scatter(data[::20, 0], data[::20, 1], label='experiment', marker="+")
ax2.legend(fontsize=5)
ax2.set_xlabel('apical rise [mm]', Fontsize=12)
ax2.set_ylabel('pressure [MPa]', Fontsize=12)

index_a = (np.abs(p_fem[str(index[0]) + str(index[1])] - 0.75*0.000133322)).argmin()
index_a_2 = (np.abs(p_fem[str(index_2[0]) + str(index_2[1])] - 0.75*0.000133322)).argmin()
a_rise_opt = apical_rise_fem[str(index[0]) + str(index[1])] - apical_rise_fem[str(index[0]) + str(index[1])][index_a]
a_rise_opt_2 = apical_rise_fem[str(index_2[0]) + str(index_2[1])] - apical_rise_fem[str(index_2[0]) + str(index_2[1])][index_a_2]
ax1.plot(a_rise_opt, p_fem[str(index[0]) + str(index[1])], label='simulation with E cost; $c_{1}$ = ' + str(np.round(c_1[index[0]], 6))
         + '[MPa]' + ' $c_{2}$ = ' + str(np.round(c_2[index[1]], 3)) + '[-]')
ax1.plot(a_rise_opt_2, p_fem[str(index_2[0]) + str(index_2[1])], label='simulation; $c_{1}$ = ' + str(np.round(c_1[index_2[0]], 6))
         + '[MPa]' + ' $c_{2}$ = ' + str(np.round(c_2[index_2[1]], 3)) + '[-]')
ax1.scatter(data[::20, 0], data[::20, 1], label='experiment', marker="+")
ax1.set_xlabel('apical rise [mm]', Fontsize=12)
ax1.set_ylabel('pressure [MPa]', Fontsize=12)
ax1.legend()
plt.show()
print(c_1[index[0]], c_2[index[1]], E_fem[str(index[0]) + str(index[1])])
