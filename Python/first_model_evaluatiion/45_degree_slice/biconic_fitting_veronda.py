import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from my_functions import *
from scipy.io import loadmat


# feb_file = {'no pre stretch': {'parameter study': 'abaqus_mesh.feb'}, 'pre stretch': {'parameter study': 'geometry_opt.feb'}}
feb_file = {'pre stretch': {'parameter study': ['', 'geometry_opt.feb']}}
nodes_disp_file = {'parameter study': 'anterior_surface_displacement.dat'}

fig1, ax1 = plt.subplots()
fig1.suptitle('Stroma: Veronda; Epithelium: Neo-Hookean', fontsize=16)

section = ['<Nodes name="Cornea"', '<NodeSet name="anterior_surface">']
file = list(nodes_disp_file.keys())[0]
k_ = np.logspace(-6, -1, 1)             # [mm⁴/(Ns)]

l_name = np.ndarray.tolist(k_) + ['fixed IOP BC']

disp_8h = np.zeros([50, 1])
for f_num in feb_file:
    if f_num == 'no pre stretch':
        num_0 = 99
        num_1 = 100
    else:
        num_0 = 0
        num_1 = 1
    for k in range(num_0, num_1):
        nodes_index = load_feb_file_nodes_id(feb_file[f_num][file], section[1])
        if k < 10:
            path2file = os.path.join('dir'+"0"+str(k), nodes_disp_file[file])
            t, nodes_t = load_output_dat_file(path2file)
        else:
            path2file = os.path.join('dir' + str(k), nodes_disp_file[file])
            t, nodes_t = load_output_dat_file(path2file)

        nodes_index = np.asarray(nodes_index)
        nodes_t = np.reshape(np.asarray(nodes_t), (len(t), len(nodes_index)*4), order='C')
        t = np.asarray(t)
        x = np.zeros((len(nodes_index), 3*len(t)))
        iii = 0
        # stitch disp and pos together
        for steps in t:
            ii = 0
            for i in nodes_index:
                temp = nodes_t[iii, int(np.where(nodes_t[iii, :] == i)[0]): int(np.where(nodes_t[iii, :] == i)[0]) + 4]
                x[ii, iii * 3:(iii + 1) * 3] = temp[0, 1:]
                ii += 1
            iii += 1
            if steps == 28800:
                disp_8h[k] = np.min(nodes_t)
        x = np.array(sorted(x, key=lambda x_column: x_column[0]))

        # revovle for spherical/biconical fit
        rot_angle = 5 / 180 * np.pi
        slices = int(2 * np.pi / rot_angle)
        skip = 5  # at least one
        index_15 = (np.abs(x[:, 0] - 1.5)).argmin()
        x_revolved = np.zeros([np.int(np.ceil(index_15 / skip)) * slices, 3 * len(t)])
        for jj in range(slices):
            R_y = np.matrix(
                [[np.round(np.cos(jj * rot_angle), decimals=6), 0, np.round(np.sin(jj * rot_angle), decimals=6)],
                 [0, 1, 0],
                 [-np.round(np.sin(jj * rot_angle), decimals=6), 0, np.round(np.cos(jj * rot_angle), decimals=6)]])
            for j in range(len(t)):
                # biconic fitting describes surface in function of z; turn coordinate system accordingly
                temp = np.transpose(np.dot(R_y, np.transpose(x[0:index_15:skip, j * 3:(j + 1) * 3])))
                temp[:, 1] = -(temp[:, 1] - np.max(temp[:, 1], axis=0))
                x_revolved[jj * np.int(np.ceil(index_15/skip)):(jj + 1)*np.int(np.ceil(index_15/skip)), j*3:(j + 1)*3] = np.concatenate([temp[:, 2], temp[:, 0], temp[:, 1]], axis=1)

        # calculate Radius and power of the eye
        n = 1.3375
        R_n = np.zeros(np.array([len(t), 1]))
        R_ny = np.zeros(np.array([len(t), 1]))
        power_eye = np.zeros(np.array([len(t), 1]))
        R = np.zeros(np.array([len(t), 1]))
        for j in range(int(np.float(len(t)))):
            pos = x_revolved[:, j * 3:(j + 1) * 3]
            r = sphere_fit(pos)
            # r = biconic_fitting(pos)
            R_n[j] = r[0]
            R_ny[j] = r[1]
            R[j] = (R_n[j] + R_ny[j])/2
            power_eye[j] = (n - 1) / (R_n[j] * 1e-3)
        # plot radius eye
        # ax1 = plt.subplot(211)
        if k < 10:
            label_name = str(l_name[k])
            ax1.plot(t/3600, power_eye - (n-1)/0.0076, label=label_name)
            leg = ax1.legend(loc='lower right', fontsize=9)
            ax1.set_xlabel('time [h]', Fontsize=12)
            ax1.set_ylabel('refractive power change [D]', Fontsize=12)
            # plt.ylim([7.6, 8.2])
            plt.xticks((np.arange(0, 22, 2)))

    ax1.plot([0, 22], [-1.74, -1.74], color='black', lw=1)
    ax1.plot([16, 16], [0, -3], color='black', lw=1)
    plt.show()