import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from my_functions import *
from scipy.io import loadmat


feb_file = {'febio': {'2 cylces': 'abaqus_mesh.feb', '2 cylces 24 h': 'abaqus_mesh.feb'}}
nodes_disp_file = {'2 cylces': 'anterior_surface_displacement.dat', '2 cylces 24 h': 'anterior_surface_displacement_24h.dat'}

plt.figure()
section = ['<Nodes name="Cornea"', '<NodeSet name="anterior_surface">']

for f_num in feb_file:
    for file, k in zip(nodes_disp_file, range(len(nodes_disp_file))):
        nodes = load_feb_file_nodes(feb_file[f_num][file], section[0])
        nodes_index = load_feb_file_nodes_id(feb_file[f_num][file], section[1])
        t, nodes_disp = load_output_dat_file(nodes_disp_file[file])
        nodes = np.asarray(nodes)
        nodes_index = np.asarray(nodes_index)
        nodes_disp = np.reshape(np.asarray(nodes_disp), (len(t), len(nodes_index)*4), order='C')
        t = np.asarray(t)
        x = np.zeros((len(nodes_index), 3*len(t)))
        iii = 0
        # stitch disp and pos together
        for steps in t:
            ii = 0
            for i in nodes_index:
                temp = nodes[np.where(nodes[:, 0] == i)[0], :] + nodes_disp[iii, int(np.where(nodes_disp[iii, :] == i)[0]): int(np.where(nodes_disp[iii, :] == i)[0]) + 4]
                x[ii, iii * 3:(iii + 1) * 3] = temp[0, 1:]
                ii += 1
            iii += 1
        x = np.array(sorted(x, key=lambda x_column: x_column[0]))

        ## revovle for spherical/biconical fit
        rot_angle = 5 / 180 * np.pi
        slices = int(2 * np.pi / rot_angle)
        skip = 10  # at least one
        index_15 = (np.abs(x[:, 0] - 1.5)).argmin()
        x_revolved = np.zeros([np.int(np.ceil(index_15 / skip)) * slices, 3 * len(t)])
        for jj in range(slices):
            R_y = np.matrix(
                [[np.round(np.cos(jj * rot_angle), decimals=6), 0, np.round(np.sin(jj * rot_angle), decimals=6)],
                 [0, 1, 0],
                 [-np.round(np.sin(jj * rot_angle), decimals=6), 0, np.round(np.cos(jj * rot_angle), decimals=6)]])
            for j in range(len(t)):
                # biconic fitting describes surface in function of z; turn coordinate system accordingly
                temp = np.transpose(np.dot(R_y, np.transpose(x[0:index_15:skip, j * 3:(j + 1) * 3])))
                temp[:, 1] = -(temp[:, 1] - np.max(temp[:, 1], axis=0))
                x_revolved[jj * np.int(np.ceil(index_15/skip)):(jj + 1)*np.int(np.ceil(index_15/skip)), j*3:(j + 1)*3] = np.concatenate([temp[:, 2], temp[:, 0], temp[:, 1]], axis=1)

        # calculate Radius and power of the eye
        n = 1.3375
        R_n = np.zeros(np.array([len(t), 1]))
        R_ny = np.zeros(np.array([len(t), 1]))
        power_eye = np.zeros(np.array([len(t), 1]))
        R = np.zeros(np.array([len(t), 1]))
        for j in range(int(np.float(len(t)))):
            pos = x_revolved[:, j * 3:(j + 1) * 3]
            r = sphere_fit(pos)
            r = biconic_fitting(pos)
            R_n[j] = r[0]
            R_ny[j] = r[1]
            R[j] = (R_n[j] + R_ny[j])/2
            power_eye[j] = (n - 1) / (R_n[j] * 1e-3)
        # plot radius eye
        ax1 = plt.subplot(211)
        ax1.plot(t/3600, R, label=f_num + ' ' + file)
        leg = plt.legend(loc='upper right')
        plt.xlabel('time [h]')
        plt.ylabel('radius [mm]')
        plt.ylim([7.6, 8.15])
        plt.xticks((np.arange(0, 50, 2)))
        # plot diopter
        ax2 = plt.subplot(212)
        ax2.plot(t/3600, power_eye)
        ax2.legend()
        plt.xlabel('time [h]')
        plt.ylabel('power [diopter]')
        plt.xticks((np.arange(0, 50, 2)))
plt.show()
