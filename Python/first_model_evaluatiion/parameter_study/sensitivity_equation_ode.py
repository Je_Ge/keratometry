import numpy as np
from scipy import integrate
import matplotlib.pyplot as plt
from my_functions import *


# ### ode to study: ### #
# consider two layers of poro elastic cuboid volume element with only top surface permeable (confined compression)

# geometric parameters
A = 6**2*np.pi                          # [mm²] top area
l_1 = 0.05                              # [mm] length
l_2 = 0.5                               # [mm] length

# mechanical parameter
k_1 = 4e-6                              # [mm⁴/(Ns)] permeability
E_1 = 0.011                             # [MPa] Young's modulus
k_2 = 8.5e-4                            # [mm⁴/(Ns)] permeability
E_2 = 0.015                             # [MPa] Young's modulus

p_10 = 0.1*np.array([k_1, k_1, E_1, E_1, l_1, l_1, k_2, k_2, E_2, E_2, l_2, l_2])
# input
f_z = -80e-3                            # [N] external force

def ode_sensitivity(t, x, k_1, k_2, E_1, E_2, l_1, l_2, area, force):
    A_11 = -k_1*E_1/l_1**2 -k_2*E_1/(l_1*l_2)
    A_12 = k_2*E_2/(l_1*l_2)
    A_21 = k_2*E_1/l_2**2
    A_22 = -k_2*E_2/l_2**2
    x_dot = np.zeros([14])
    x_dot[0] = A_11*x[0] + A_12*x[1] + k_1*force/(area*l_1**2)                                    # x_0 = epsilon_epi
    x_dot[1] = A_21*x[0] + A_22*x[1]                                                              # x_1 = epsilon_stroma

    x_dot[2] = A_11*x[2] + A_11*x[3] + (-E_1*x[0] + force/area)/l_1**2
    x_dot[3] = A_21*x[2] + A_22*x[3]
    x_dot[4] = A_11*x[4] + A_11*x[5] - (k_1/l_1**2+k_2/(l_1*l_2))*x[0]
    x_dot[5] = A_21*x[4] + A_22*x[5] + k_2/l_2**2*x[0]
    x_dot[6] = A_11*x[6] + A_11*x[7] + 2*k_1/l_1**3*(E_1*x[0] + force/area) + k_2/(l_1**2*l_2)*(E_1*x[0]-E_2*x[1])
    x_dot[7] = A_21*x[6] + A_22*x[7]
    x_dot[8] = A_11*x[8] + A_11*x[9] - (E_1*x[0]-E_2*x[1])/(l_1*l_2) 
    x_dot[9] = A_21*x[8] + A_22*x[9] + (E_1*x[0]-E_2*x[1])/l_2**2
    x_dot[10] = A_11*x[10] + A_11*x[11] + k_2/(l_1*l_2)*x[1]
    x_dot[11] = A_21*x[10] + A_22*x[11] - k_2/l_2**2*x[1]
    x_dot[12] = A_11*x[12] + A_11*x[13] + k_2/(l_1*l_2**2)*(E_1*x[0]-E_2*x[1])
    x_dot[13] = A_21*x[12] + A_22*x[13] - 2*k_2/l_2**3*(E_1*x[0] - E_2*x[1])
    return x_dot


fig, (ax1, ax2, ax3) = plt.subplots(3, 1, sharex=True)
t0, t1, t2 = 0, 28800, 72000                                 # start and end
t = np.zeros([200])
t[1:98] = np.logspace(-1, 4, 97)
t[99] = 28800
t[100:198] = np.logspace(-1, 4, 98) + t1
t[198] = 40000
t[199] = t2                                               # the points of evaluation of solution
t_1 = np.linspace(t0, t1, 1000)
t_2 = np.linspace(t1, t2, 1000)
t = np.concatenate((t_1, t_2[1:]), axis=0)                   # the points of evaluation of solution
x_0 = np.zeros([14])                                         # initial value
x = np.zeros([len(t), len(x_0)])                             # array for solution
x[0, :] = x_0
r = integrate.ode(ode_sensitivity).set_integrator("dopri5")  # choice of method
r.set_initial_value(x_0, t0).set_f_params(k_1, k_2, E_1, E_2, l_1, l_2, A, f_z)               # initial values
for i in range(1, t.size):
    if t[i] == 28800:
        r.set_f_params(k_1, k_2, E_1, E_2, l_1, l_2, A, 0)
    x[i, :] = r.integrate(t[i])                               # get one more value, add it to the array
    if not r.successful():
        raise RuntimeError("Could not integrate")

t_fem, nodes_disp_top = load_output_dat_file('top.dat')
t_fem, nodes_disp_middle = load_output_dat_file('middle.dat')
t_fem = np.asarray(t_fem)
nodes_disp_top = np.reshape(np.asarray(nodes_disp_top), (len(t_fem), 4), order='C')
nodes_disp_middle = np.reshape(np.asarray(nodes_disp_middle), (len(t_fem), 4), order='C')
eps_epi_fem = -(nodes_disp_top[:, -1] + nodes_disp_middle[:, -1])/l_1
eps_str_fem = -(nodes_disp_middle[:, -1])/l_2

[a, b] = ax1.plot(t/3600, x[:, 0:2])
fe1 = ax1.plot(t_fem/3600, eps_epi_fem)
fe2 = ax1.plot(t_fem/3600, eps_str_fem)
cm = plt.get_cmap('tab20')
ax2.set_color_cycle([cm(1.*i/12) for i in range(12)])
[c, d, e, f, g, h] = ax2.plot(t/3600, np.multiply(x[:, 2:8], p_10[0:6]))
[i, j, k, l, m, n] = ax3.plot(t/3600, np.multiply(x[:, 8:], p_10[6:]))
fig.suptitle("Sensitivity of a poro-elastic compression of two layers with thickness ratio 10:1", fontsize="x-large")
ax1.set_title('strain of the epithelium and stroma', Fontsize=12)
ax1.legend([a, b, fe1, fe2], ["$\epsilon_{epithelium}$", "$\epsilon_{stroma}$", "$\epsilon_{epithelium}$ febio", "$\epsilon_{stroma}$ febio" ], loc='lower left')
ax1.set_ylabel('strain [-]', Fontsize=12)
ax2.set_title('10% variation of $\lambda_{epithelium}$', Fontsize=12)
ax2.legend([c, d, e, f, g, h], ["$\Delta k_1$ on $\epsilon_{epithelium}$", "$\Delta k_1$ on $\epsilon_{stroma}$",
                                "$\Delta E_1$ on $\epsilon_{epithelium}$", "$\Delta E_1$ on $\epsilon_{stroma}$",
                                "$\Delta l_1$ on $\epsilon_{epithelium}$", "$\Delta l_1$ on $\epsilon_{stroma}$"
                                ], loc='lower right', fontsize=9, labelspacing=0.2)
ax3.legend([i, j, k, l, m, n], ["$\Delta k_2$ on $\epsilon_{epithelium}$", "$\Delta k_2$ on $\epsilon_{stroma}$",
                                "$\Delta E_2$ on $\epsilon_{epithelium}$", "$\Delta E_2$ on $\epsilon_{stroma}$",
                                "$\Delta l_2$ on $\epsilon_{epithelium}$", "$\Delta l_2$ on $\epsilon_{stroma}$"
                                ], loc='lower right', fontsize=9, labelspacing=0.2)
ax2.set_ylabel('change in strain [$\Delta$ $\epsilon$]', Fontsize=12)
ax3.set_title('10% variation of $\lambda_{stroma}$', Fontsize=12)
ax3.set_xlabel('time [h]', Fontsize=12)
ax3.set_ylabel('change in strain [$\Delta$ $\epsilon$]', Fontsize=12)
plt.show()


l_1 = 0.55/2                               # [mm] length
l_2 = 0.55/2                               # [mm] length
fig, (ax1, ax2, ax3) = plt.subplots(3, 1, sharex=True)
t0, t1, t2 = 0, 28800, 72000                                 # start and end
t = np.zeros([200])
t[1:98] = np.logspace(-1, 4, 97)
t[99] = 28800
t[100:198] = np.logspace(-1, 4, 98) + t1
t[198] = 40000
t[199] = t2                                               # the points of evaluation of solution
t_1 = np.linspace(t0, t1, 1000)
t_2 = np.linspace(t1, t2, 1000)
t = np.concatenate((t_1, t_2[1:]), axis=0)                   # the points of evaluation of solution
x_0 = np.zeros([14])                                         # initial value
x = np.zeros([len(t), len(x_0)])                             # array for solution
x[0, :] = x_0
r = integrate.ode(ode_sensitivity).set_integrator("dopri5")  # choice of method
r.set_initial_value(x_0, t0).set_f_params(k_1, k_2, E_1, E_2, l_1, l_2, A, f_z)               # initial values
for i in range(1, t.size):
    if t[i] == 28800:
        r.set_f_params(k_1, k_2, E_1, E_2, l_1, l_2, A, 0)
    x[i, :] = r.integrate(t[i])                               # get one more value, add it to the array
    if not r.successful():
        raise RuntimeError("Could not integrate")
[a, b] = ax1.plot(t/3600, x[:, 0:2])
cm = plt.get_cmap('tab20')
ax2.set_color_cycle([cm(1.*i/12) for i in range(12)])
[c, d, e, f, g, h] = ax2.plot(t/3600, np.multiply(x[:, 2:8], p_10[0:6]))
[i, j, k, l, m, n] = ax3.plot(t/3600, np.multiply(x[:, 8:], p_10[6:]))
fig.suptitle("Sensitivity of a poro-elastic compression of two layers with thickness ratio 1:1", fontsize="x-large")
ax1.set_title('strain of the epithelium and stroma', Fontsize=12)
ax1.legend([a, b], ["$\epsilon_{epithelium}$", "$\epsilon_{stroma}$"], loc='lower left')
ax1.set_ylabel('strain [-]', Fontsize=12)
ax2.set_title('10% variation of $\lambda_{epithelium}$', Fontsize=12)
ax2.legend([c, d, e, f, g, h], ["$\Delta k_1$ on $\epsilon_{epithelium}$", "$\Delta k_1$ on $\epsilon_{stroma}$",
                                "$\Delta E_1$ on $\epsilon_{epithelium}$", "$\Delta E_1$ on $\epsilon_{stroma}$",
                                "$\Delta l_1$ on $\epsilon_{epithelium}$", "$\Delta l_1$ on $\epsilon_{stroma}$"
                                ], loc='lower right', fontsize=9, labelspacing=0.2)
ax3.legend([i, j, k, l, m, n], ["$\Delta k_2$ on $\epsilon_{epithelium}$", "$\Delta k_2$ on $\epsilon_{stroma}$",
                                "$\Delta E_2$ on $\epsilon_{epithelium}$", "$\Delta E_2$ on $\epsilon_{stroma}$",
                                "$\Delta l_2$ on $\epsilon_{epithelium}$", "$\Delta l_2$ on $\epsilon_{stroma}$"
                                ], loc='lower right', fontsize=9, labelspacing=0.2)
ax2.set_ylabel('change in strain [$\Delta$ $\epsilon$]', Fontsize=12)
ax3.set_title('10% variation of $\lambda_{stroma}$', Fontsize=12)
ax3.set_xlabel('time [h]', Fontsize=12)
ax3.set_ylabel('change in strain [$\Delta$ $\epsilon$]', Fontsize=12)
plt.show()
