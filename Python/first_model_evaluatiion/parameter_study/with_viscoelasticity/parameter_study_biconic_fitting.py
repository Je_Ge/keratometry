import os

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from my_functions import *
from scipy.io import loadmat


feb_file = {'febio': {'parameter study': 'abaqus_mesh.feb'}}
nodes_disp_file = {'parameter study': 'anterior_surface_displacement.dat'}

fig1, ax1 = plt.subplots()
fig1.suptitle('k = 4.64158883e-04 mm⁴/(Ns): change in Young\'s Modulus')
fig2, ax2 = plt.subplots()
fig2.suptitle('E = 11 kPa: change in permeability')
fig3, ax3 = plt.subplots()
fig3.suptitle('E = 11 kPa k = 1e-06: change in $\gamma$')
fig4, ax4 = plt.subplots()
fig4.suptitle("E = 11 kPa k = 1e-06: change in $tau$")
fig5, ax5 = plt.subplots()
fig5.suptitle("E = 11 kPa k = 1e-06: change in $tau$")
fig6, ax6 = plt.subplots()
fig6.suptitle("E = 11 kPa k = 1e-06: change in k Bowman\'s membrane")
section = ['<Nodes name="Cornea"', '<NodeSet name="anterior_surface">']
file = list(nodes_disp_file.keys())[0]
E = np.round(np.linspace(5, 25, 10), 1)         # [kPa]
k_ = np.round(np.logspace(-6, -3, 10), 6)       # [mm⁴/(Ns)]
gamma = np.linspace(0.25, 3, 10)                # [MPa]
tau = np.zeros([23, 1])
tau[0:10, 0] = np.linspace(50, 200, 10)         # [s]
tau[10, 0] = 7500                               # [s]
tau[11, 0] = 15000                              # [s]
tau[12, 0] = 30000                              # [s]
tau = tau.astype(str)
tau[13][0] = '30000 + 150'                               # [s]
tau[14][0] = '30000 + 75000'                             # [s]
tau[15][0] = '30000 + 75000 (with both halfed $\gamma$)' # [s]
tau[16][0] = '750000'                                    # [s]
tau[17][0] = 'Bowman membrane k = 5*10⁻² mm⁴/(Ns)'       # [s]
tau[18][0] = 'Bowman membrane k = 10⁻⁴ mm⁴/(Ns)'         # [s]
tau[19][0] = 'Bowman membrane k = 5*10⁻⁷ mm⁴/(Ns)'       # [s]
tau[20][0] = 'Bowman membrane k = 5*10⁻⁸ mm⁴/(Ns)'       # [s]
tau[21][0] = 'Bowman membrane k = 10⁻⁹ mm⁴/(Ns)'         # [s]
tau[22][0] = 'Bowman membrane k = 10⁻¹⁸ mm⁴/(Ns)'         # [s]
disp_8h = np.zeros([53, 1])
for f_num in feb_file:
    for k in range(40, 53):
        nodes = load_feb_file_nodes(feb_file[f_num][file], section[0])
        nodes_index = load_feb_file_nodes_id(feb_file[f_num][file], section[1])
        if k < 10:
            path2file = os.path.join('dir'+"0"+str(k), nodes_disp_file[file])
            t, nodes_disp = load_output_dat_file(path2file)
            #t, nodes_disp = load_output_dat_file('parameterstudy_k_5e-2fixed_E_9dot8fixed/'+'dir'+"0"+str(k)+"/"+nodes_disp_file[file])

        else:
            path2file = os.path.join('dir' + str(k), nodes_disp_file[file])
            t, nodes_disp = load_output_dat_file(path2file)
            # t, nodes_disp = load_output_dat_file('parameterstudy_k_5e-2fixed_E_9dot8fixed/'+'dir'+str(k)+"/"+nodes_disp_file[file])
        nodes = np.asarray(nodes)
        nodes_index = np.asarray(nodes_index)
        nodes_disp = np.reshape(np.asarray(nodes_disp), (len(t), len(nodes_index)*4), order='C')
        t = np.asarray(t)
        x = np.zeros((len(nodes_index), 3*len(t)))
        iii = 0
        # stitch disp and pos together
        for steps in t:
            ii = 0
            for i in nodes_index:
                temp = nodes[np.where(nodes[:, 0] == i)[0], :] + nodes_disp[iii, int(np.where(nodes_disp[iii, :] == i)[0]): int(np.where(nodes_disp[iii, :] == i)[0]) + 4]
                x[ii, iii * 3:(iii + 1) * 3] = temp[0, 1:]
                ii += 1
            iii += 1
            if steps == 28800:
                disp_8h[k] = np.min(nodes_disp)
        x = np.array(sorted(x, key=lambda x_column: x_column[0]))

        # revovle for spherical/biconical fit
        rot_angle = 5 / 180 * np.pi
        slices = int(2 * np.pi / rot_angle)
        skip = 10  # at least one
        index_15 = (np.abs(x[:, 0] - 1.5)).argmin()
        x_revolved = np.zeros([np.int(np.ceil(index_15 / skip)) * slices, 3 * len(t)])
        for jj in range(slices):
            R_y = np.matrix(
                [[np.round(np.cos(jj * rot_angle), decimals=6), 0, np.round(np.sin(jj * rot_angle), decimals=6)],
                 [0, 1, 0],
                 [-np.round(np.sin(jj * rot_angle), decimals=6), 0, np.round(np.cos(jj * rot_angle), decimals=6)]])
            for j in range(len(t)):
                # biconic fitting describes surface in function of z; turn coordinate system accordingly
                temp = np.transpose(np.dot(R_y, np.transpose(x[0:index_15:skip, j * 3:(j + 1) * 3])))
                temp[:, 1] = -(temp[:, 1] - np.max(temp[:, 1], axis=0))
                x_revolved[jj * np.int(np.ceil(index_15/skip)):(jj + 1)*np.int(np.ceil(index_15/skip)), j*3:(j + 1)*3] = np.concatenate([temp[:, 2], temp[:, 0], temp[:, 1]], axis=1)

        # calculate Radius and power of the eye
        n = 1.3375
        R_n = np.zeros(np.array([len(t), 1]))
        R_ny = np.zeros(np.array([len(t), 1]))
        power_eye = np.zeros(np.array([len(t), 1]))
        R = np.zeros(np.array([len(t), 1]))
        for j in range(int(np.float(len(t)))):
            pos = x_revolved[:, j * 3:(j + 1) * 3]
            r = sphere_fit(pos)
            # r = biconic_fitting(pos)
            R_n[j] = r[0]
            R_ny[j] = r[1]
            R[j] = (R_n[j] + R_ny[j])/2
            power_eye[j] = (n - 1) / (R_n[j] * 1e-3)
        # plot radius eye
        # ax1 = plt.subplot(211)
        if k < 10:
            label_name = str(E[k]) + ' kPa'
            ax1.plot(t/3600, power_eye - (n-1)/0.0076, label=label_name)
            leg = ax1.legend(loc='lower right', fontsize=7)
            ax1.set_xlabel('time [h]')
            ax1.set_ylabel('refractive power change [D]')
            # plt.ylim([7.6, 8.2])
            plt.xticks((np.arange(0, 22, 2)))
        elif k < 20:
            label_name = str(k_[k-20]) + 'mm⁴/(Ns)'
            #ax2.plot(t / 3600, R_n, label=label_name)
            ax2.plot(t/3600, power_eye - (n-1)/0.0076, label=label_name)
            leg = ax2.legend(loc='lower right', fontsize=7)
            ax2.set_xlabel('time [h]')
            ax2.set_ylabel('refractive power change [D]')
            # plt.ylim([7.6, 8.2])
            plt.xticks((np.arange(0, 22, 2)))
        elif k < 30:
            label_name = str(gamma[k-20]) + 'MPa'
            # ax3.plot(t / 3600, R_n, label=label_name)
            ax3.plot(t / 3600, power_eye - (n - 1) / 0.0076, label=label_name)
            leg = ax3.legend(loc='lower right', fontsize=7)
            ax3.set_xlabel('time [h]')
            ax3.set_ylabel('refractive power change [D]')
            # plt.ylim([7.6, 8.2])
            plt.xticks((np.arange(0, 22, 2)))

        elif k<40:
            label_name = tau[k-30][0] + 's'
            # ax4.plot(t / 3600, R_n, label=label_name)
            ax4.plot(t / 3600, power_eye - (n - 1) / 0.0076, label=label_name)
            leg = ax4.legend(loc='lower right', fontsize=7)
            ax4.set_xlabel('time [h]')
            ax4.set_ylabel('refractive power change [D]')
            # plt.ylim([7.6, 8.2])
            plt.xticks((np.arange(0, 22, 2)))
        if 40 <= k < 47:
            label_name = tau[k - 30][0] + 's'
            # ax4.plot(t / 3600, R_n, label=label_name)
            ax5.plot(t / 3600, power_eye - (n - 1) / 0.0076, label=label_name)
            leg = ax5.legend(loc='lower right', fontsize=7)
            ax5.set_xlabel('time [h]')
            ax5.set_ylabel('refractive power change [D]')
            # plt.ylim([7.6, 8.2])
            plt.xticks((np.arange(0, 22, 2)))
        if 46 <= k:
            label_name = tau[k - 30][0] + 's'
            # ax4.plot(t / 3600, R_n, label=label_name)
            ax6.plot(t / 3600, power_eye - (n - 1) / 0.0076, label=label_name)
            leg = ax6.legend(loc='lower right', fontsize=7)
            ax6.set_xlabel('time [h]')
            ax6.set_ylabel('refractive power change [D]')
            # plt.ylim([7.6, 8.2])
            plt.xticks((np.arange(0, 22, 2)))
        # plt.plot(t/3600, R_n, label=label_name)
        # leg = plt.legend(loc='upper right', fontsize=7)
        # plt.xlabel('time [h]')
        # plt.ylabel('radius [mm]')
        # plt.ylim([7.6, 8.2])
        # plt.xticks((np.arange(0, 22, 2)))
        # plot diopter
        # ax2 = plt.subplot(212)
        # ax2.plot(t/3600, power_eye)
        # # ax2.legend()
        # plt.xlabel('time [h]')
        # plt.ylabel('power [diopter]')
        # plt.xticks((np.arange(0, 22, 2)))
    # plt.show()
    ax1.plot([0, 22], [-1.74, -1.74], color='black')
    ax1.plot([16, 16], [0, -3], color='black')
    ax2.plot([0, 22], [-1.74, -1.74], color='black')
    ax2.plot([16, 16], [0, -3], color='black')
    ax3.plot([0, 22], [-1.74, -1.74], color='black')
    ax3.plot([16, 16], [0, -3], color='black')
    ax4.plot([0, 22], [-1.74, -1.74], color='black')
    ax4.plot([16, 16], [0, -3], color='black')
    ax5.plot([0, 22], [-1.74, -1.74], color='black')
    ax5.plot([16, 16], [0, -3], color='black')
    ax6.plot([0, 22], [-1.74, -1.74], color='black')
    ax6.plot([16, 16], [0, -3], color='black')
    fig1.show()
    fig2.show()
    fig3.show()
    fig4.show()
