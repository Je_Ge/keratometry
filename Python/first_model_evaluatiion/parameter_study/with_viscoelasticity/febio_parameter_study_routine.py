import os as os
import numpy as np

#### define fixed values ####
nu = 0.075                              # [-]
gamma = 1.25                            # [MPa]
tau = 50                                # [s]
#### Stromal value ####
E_stroma = 15e-3                        # [MPa]
k_stroma = 5e-3                         # [mm⁴/(Ns)]
#### Epithleial values ####
E = np.linspace(2e-3, 25e-3, 10)
k = np.logspace(-6, -1, 10)

os.chdir("/mnt/BackUp/jeremy/FEBIO/parameter_study/")  # get and create file in FEBio folder

#### creat must points ####
# step one
f = open("must_point_curve_1.feb", "w+")
f.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n")
f.write("<febio_spec version=\"2.5\">\n")
f.write("\t<LoadData>\n")
f.write("\t\t<loadcurve id=\"3\" type=\"linear\">\n")
step = 10
time = np.zeros([step, 1])
time[1] = 10
time[2] = 100
max_time_step = 1000
max_time = np.ones([step, 1])*max_time_step
max_time[0] = 0.05
max_time[1] = 2
max_time[2] = 50

for j in range(step):
    if j > 2:
        time[j] = j/step*28800
        if j < 2*step/3:
            max_time[j] = 2*max_time_step/3*(1 - np.exp(-j*0.075))
    f.write("\t\t\t<loadpoint>" + str(time[j, 0]) + ", " + str(max_time[j, 0]) + "</loadpoint>\n")
f.write("\t\t</loadcurve>\n")
f.write("\t</LoadData>\n")
f.write("</febio_spec>")
f.close()

# step two
f = open("must_point_curve_2.feb", "w+")
f.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n")
f.write("<febio_spec version=\"2.5\">\n")
f.write("\t<LoadData>\n")
f.write("\t\t<loadcurve id=\"4\" type=\"linear\">\n")
step = 10
time = np.zeros([step, 1])
max_time_step = 500
time[1] = 10
time[2] = 100
max_time = np.ones([step, 1])*max_time_step
max_time[0] = 0.05
max_time[1] = 2
max_time[2] = 10
for j in range(step):
    if j > 2:
        time[j] = j/step*43200
        if j < 2*step/3:
            max_time[j] = 2*max_time_step/3*(1 - np.exp(-j*0.075))
    f.write("\t\t\t<loadpoint>" + str(time[j, 0]) + ", " + str(max_time[j, 0]) + "</loadpoint>\n")
f.write("\t\t</loadcurve>\n")
f.write("\t</LoadData>\n")
f.write("</febio_spec>")
f.close()

#### run febio routine ####
for i in range(1):  # 2
    for ii in range(len(E)):  # len(E)):
        f = open("parameters.feb", "w+")
        f.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n")
        f.write("<febio_spec version=\"2.5\">\n")
        f.write("\t<Parameters>\n")
        f.write("\t\t<param name=\"young_stroma\">"+str(E_stroma)+"</param>\n")
        f.write("\t\t<param name=\"permeability_stroma\">"+str(k_stroma)+"</param>\n")
        if i == 0:
            f.write("\t\t<param name=\"young_epi\">" + str(E[ii]) + "</param>\n")
            f.write("\t\t<param name=\"permeability_epi\">" + str(k[3]) + "</param>\n")
        else:
            f.write("\t\t<param name=\"young_epi\">" + str(E[int(np.floor(len(E)/2))]) + "</param>\n")
            f.write("\t\t<param name=\"permeability_epi\">" + str(k[ii]) + "</param>\n")
        f.write("\t\t<param name=\"poison\">" + str(nu) + "</param>\n")
        f.write("\t\t<param name=\"gamma_1\">" + str(gamma) + "</param>\n")
        f.write("\t\t<param name=\"tau_1\">" + str(tau) + "</param>\n")
        f.write("\t</Parameters>\n")
        f.write("</febio_spec>")
        f.close()
        os.system("febio2 -i parameter_study.feb")
        os.system("mkdir /mnt/BackUp/jeremy/repository/keratometry/Python/first_model_evaluatiion/parameter_study/dir"+str(i)+str(ii))
        os.system(("mv anterior_surface_displacement.dat /mnt/BackUp/jeremy/repository/keratometry/Python/first_model_evaluatiion/parameter_study/dir"+str(i)+str(ii)))
        print(10*i+ii)
