import os as os
import numpy as np

#### Stromal value ####
E_stroma = 15e-3                        # [MPa]
k_stroma = 5e-3                         # [mm⁴/(Ns)]
nu_stroma = 0.075                       # [-]
gamma_stroma = 1.25                     # [MPa]
tau_stroma = 50                         # [s]
#### Epithleial values ####
E = np.linspace(5e-3, 25e-3, 10)        # [MPa]
k = np.logspace(-6, -3, 10)             # [mm'4/(Ns)]
gamma = np.linspace(0.25, 3, 10)        # [MPa]
tau = np.zeros([11, 1])
tau[0:10, 0] = np.linspace(50, 200, 10) # [s]
tau[-1, 0] = 2*15000                      # [s]
nu = 0.075                              # [-]
#### displacment after 8 h for E: stroma values and k[-2]; k: E[3] and  stroma val; gamma: E[3], k[0], stroma; tau: E[3], k[0], gamma[0] stroma val
unload_disp = np.array([[-0.0309588],
       [-0.0312901],
       [-0.0328116],
       [-0.0343411],
       [-0.0345654],
       [-0.0352118],
       [-0.0354556],
       [-0.0355309],
       [-0.0357919],
       [-0.035809],
       [-0.025325],
       [-0.0261619],
       [-0.0255679],
       [-0.026497],
       [-0.0262734],
       [-0.0272145],
       [-0.0288036],
       [-0.0312008],
       [-0.0347857],
       [-0.0393761],
       [-0.0253195],
       [-0.0253238],
       [-0.0253202],
       [-0.0253192],
       [-0.0260051],
       [-0.0260389],
       [-0.0253205],
       [-0.0253205],
       [-0.0253205],
       [-0.025322],
       [-0.0253198],
       [-0.025319],
       [-0.0253199],
       [-0.0253199],
       [-0.0253198],
       [-0.0253181],
       [-0.0253194],
       [-0.0253196],
       [-0.025348],
       [-0.0253198],
       [-0.0253198]])
####
os.chdir("/mnt/BackUp/jeremy/FEBIO/parameter_study/")  # get and create file in FEBio folder

#### creat must points ####
# step one
f = open("must_point_curve_1.feb", "w+")
f.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n")
f.write("<febio_spec version=\"2.5\">\n")
f.write("\t<LoadData>\n")
f.write("\t\t<loadcurve id=\"4\" type=\"linear\"extend=\"constant\">\n")
step = 10
time = np.zeros([step, 1])
time[1] = 10
time[2] = 1200
time[-3] = 28749
time[-2] = 24000
time[-1] = 28800
max_time_step = 1500
max_time = np.ones([step, 1])*max_time_step
max_time[0] = 0.1
max_time[1] = 2.5
max_time[2] = 250
# max_time[-1] = 10
# max_time[-2] = 0.05
for j in range(step):
    if j > 2 and j < step-3:
        time[j] = j/step*28800
        if j < 2*step/3:
            max_time[j] = 2*max_time_step/3*(1 - np.exp(-j*0.75))
    f.write("\t\t\t<loadpoint>" + str(time[j, 0]) + ", " + str(max_time[j, 0]) + "</loadpoint>\n")
f.write("\t\t</loadcurve>\n")
f.write("\t</LoadData>\n")
f.write("</febio_spec>")
f.close()

# step two
f = open("must_point_curve_2.feb", "w+")
f.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n")
f.write("<febio_spec version=\"2.5\">\n")
f.write("\t<LoadData>\n")
f.write("\t\t<loadcurve id=\"5\" type=\"linear\" extend=\"constant\">\n")
step = 10
time = np.ones([step, 1])*28800
max_time_step = 1500
time[1] = 28810
time[2] = 500+28800
max_time = np.ones([step, 1])*max_time_step
max_time[0] = 0.1
max_time[1] = 1
max_time[2] = 250
for j in range(step):
    if j > 2:
        time[j] = j/step*43200+28800
        if j < 2*step/3:
            max_time[j] = 2*max_time_step/3*(1 - np.exp(-j*0.75))
    f.write("\t\t\t<loadpoint>" + str(time[j, 0]) + ", " + str(max_time[j, 0]) + "</loadpoint>\n")
f.write("\t\t</loadcurve>\n")
f.write("\t</LoadData>\n")
f.write("</febio_spec>")
f.close()

#### run febio routine ####
# oldunload curve for permeability variations
# unload_disp = [-0.025]  # [-0.0247, -0.025, -0.025, -0.026, -0.0265, -0.0275, -0.029, -0.0295, -0.033, -0.0385]
for i in range(0, 1):  # 4
    for ii in range(4):  # len(E)):
        f = open("unload_curve.feb", "w+")
        f.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n")
        f.write("<febio_spec version=\"2.5\">\n")
        f.write("\t<LoadData>\n")
        f.write("\t\t<loadcurve id=\"3\" type=\"linear\" extend=\"constant\">\n")
        f.write("\t\t\t<loadpoint>" "28800," + str(unload_disp[ii+i*10, 0]) + "</loadpoint>\n")
        # f.write("\t\t\t<loadpoint>" "28850," + str(0.90*unload_disp[ii + i * 10, 0]-0.005) + "</loadpoint>\n")
        # f.write("\t\t\t<loadpoint>" "28950," + str(0.65*0.01) + "</loadpoint>\n")
        f.write("\t\t\t<loadpoint>" "28890, 0.01" + "</loadpoint>\n")
        f.write("\t\t</loadcurve>\n")
        f.write("\t</LoadData>\n")
        f.write("</febio_spec>")
        f.close()

        f = open("parameters.feb", "w+")
        f.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n")
        f.write("<febio_spec version=\"2.5\">\n")
        f.write("\t<Parameters>\n")
        f.write("\t\t<param name=\"young_stroma\">"+str(E_stroma)+"</param>\n")
        f.write("\t\t<param name=\"permeability_stroma\">"+str(k_stroma)+"</param>\n")
        f.write("\t\t<param name=\"poison_stroma\">" + str(nu_stroma) + "</param>\n")
        f.write("\t\t<param name=\"gamma_stroma\">" + str(gamma_stroma) + "</param>\n")
        f.write("\t\t<param name=\"tau_stroma\">" + str(tau_stroma) + "</param>\n")
        if i == 0:
            f.write("\t\t<param name=\"young_epi\">" + str(E[ii]) + "</param>\n")
            f.write("\t\t<param name=\"permeability_epi\">" + str(k[-2]) + "</param>\n")
            f.write("\t\t<param name=\"poison_epi\">" + str(nu) + "</param>\n")
            f.write("\t\t<param name=\"gamma_epi\">" + str(gamma_stroma) + "</param>\n")
            f.write("\t\t<param name=\"tau_epi\">" + str(tau_stroma) + "</param>\n")
        elif i == 1:
            f.write("\t\t<param name=\"young_epi\">" + str(E[3]) + "</param>\n")
            f.write("\t\t<param name=\"permeability_epi\">" + str(k[ii]) + "</param>\n")
            f.write("\t\t<param name=\"poison_epi\">" + str(nu) + "</param>\n")
            f.write("\t\t<param name=\"gamma_epi\">" + str(gamma_stroma) + "</param>\n")
            f.write("\t\t<param name=\"tau_epi\">" + str(tau_stroma) + "</param>\n")
        elif i == 2:
            f.write("\t\t<param name=\"young_epi\">" + str(E[3]) + "</param>\n")
            f.write("\t\t<param name=\"permeability_epi\">" + str(k[0]) + "</param>\n")
            f.write("\t\t<param name=\"poison_epi\">" + str(nu) + "</param>\n")
            f.write("\t\t<param name=\"gamma_epi\">" + str(gamma[ii]) + "</param>\n")
            f.write("\t\t<param name=\"tau_epi\">" + str(tau_stroma) + "</param>\n")
        else:
            f.write("\t\t<param name=\"young_epi\">" + str(E[3]) + "</param>\n")
            f.write("\t\t<param name=\"permeability_epi\">" + str(k[0]) + "</param>\n")
            f.write("\t\t<param name=\"poison_epi\">" + str(nu) + "</param>\n")
            f.write("\t\t<param name=\"gamma_epi\">" + str(gamma[3]) + "</param>\n")
            f.write("\t\t<param name=\"tau_epi\">" + str(tau[ii][0]) + "</param>\n")
        f.write("\t</Parameters>\n")
        f.write("</febio_spec>")
        f.close()
        if True:  # i != 1 or ii == 7:
            os.system("febio2 -i parameter_study.feb")
            os.system("mkdir /mnt/BackUp/jeremy/repository/keratometry/Python/first_model_evaluatiion/parameter_study/dir"+str(i)+str(ii))
            os.system(("mv anterior_surface_displacement.dat /mnt/BackUp/jeremy/repository/keratometry/Python/first_model_evaluatiion/parameter_study/dir"+str(i)+str(ii)))
            os.system("mkdir dir" + str(i) + str(ii))
            os.system(("cp parameter_study.xplt dir" + str(i) + str(ii) + "/parameter_study.xplt"))
            os.system(("cp parameters.feb dir" + str(i) + str(ii) + "/parameters.feb"))

