E = np.round(np.linspace(5, 25, 10), 1)         # [kPa]			k[-2]
k_ = np.round(np.logspace(-6, -3, 10), 6)       # [mm⁴/(Ns)]		E[5]
gamma = np.linspace(0.25, 3, 10)                # [MPa]			k[-2] E[5]
tau = np.linspace(50, 200, 10)                  # [s]			k[-2] E[5]   (not denoted parameters are the same as the ones from the stroma)
