import os as os
import numpy as np

# ### Stromal value ### #
E_stroma = 15e-3                        # [MPa]
k_stroma = 8.5e-4                       # [mm⁴/(Ns)]
nu_stroma = 0.075                       # [-]
M_stroma = np.linspace(0.2, 7, 10)
alpha_stroma = np.logspace(-3, 1, 10)
# ### Epithleial values ### #
E = np.linspace(5e-3, 25e-3, 10)        # [MPa]
k = np.logspace(-6, -3, 10)             # [mm⁴/(Ns)]
nu = 0.075                              # [-]
M = np.linspace(0.2, 7, 10)
alpha = np.logspace(-3, 1, 10)
unload_disp = -0.025
####
os.chdir("/mnt/BackUp/jeremy/FEBIO/parameter_study/")  # get and create file in FEBio folder

#### creat must points ####
# step one
f = open("must_point_curve_1.feb", "w+")
f.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n")
f.write("<febio_spec version=\"2.5\">\n")
f.write("\t<LoadData>\n")
f.write("\t\t<loadcurve id=\"4\" type=\"linear\"extend=\"constant\">\n")
step = 10
time = np.zeros([step, 1])
time[1] = 10
time[2] = 1200
time[-3] = 28749
time[-2] = 24000
time[-1] = 28800
max_time_step = 1500
max_time = np.ones([step, 1])*max_time_step
max_time[0] = 0.1
max_time[1] = 1
max_time[2] = 100
# max_time[-1] = 10
# max_time[-2] = 0.05
for j in range(step):
    if j > 2 and j < step-3:
        time[j] = j/step*28800
        if j < 2*step/3:
            max_time[j] = 2*max_time_step/3*(1 - np.exp(-j*0.75))
    f.write("\t\t\t<loadpoint>" + str(time[j, 0]) + ", " + str(max_time[j, 0]) + "</loadpoint>\n")
f.write("\t\t</loadcurve>\n")
f.write("\t</LoadData>\n")
f.write("</febio_spec>")
f.close()

# step two
f = open("must_point_curve_2.feb", "w+")
f.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n")
f.write("<febio_spec version=\"2.5\">\n")
f.write("\t<LoadData>\n")
f.write("\t\t<loadcurve id=\"5\" type=\"linear\" extend=\"constant\">\n")
step = 10
time = np.ones([step, 1])*28800
max_time_step = 1500
time[1] = 28810
time[2] = 500+28800
max_time = np.ones([step, 1])*max_time_step
max_time[0] = 0.05
max_time[1] = 1
max_time[2] = 100
for j in range(step):
    if j > 2:
        time[j] = j/step*43200+28800
        if j < 2*step/3:
            max_time[j] = 2*max_time_step/3*(1 - np.exp(-j*0.75))
    f.write("\t\t\t<loadpoint>" + str(time[j, 0]) + ", " + str(max_time[j, 0]) + "</loadpoint>\n")
f.write("\t\t</loadcurve>\n")
f.write("\t</LoadData>\n")
f.write("</febio_spec>")
f.close()

#### run febio routine ####
# oldunload curve for permeability variations
# unload_disp = [-0.025]  # [-0.0247, -0.025, -0.025, -0.026, -0.0265, -0.0275, -0.029, -0.0295, -0.033, -0.0385]
for i in range(2, 4):  # 4
    for ii in range(len(E)):
        f = open("unload_curve.feb", "w+")
        f.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n")
        f.write("<febio_spec version=\"2.5\">\n")
        f.write("\t<LoadData>\n")
        f.write("\t\t<loadcurve id=\"3\" type=\"linear\" extend=\"constant\">\n")
        f.write("\t\t\t<loadpoint>" "28800," + str(unload_disp) + "</loadpoint>\n")
        # f.write("\t\t\t<loadpoint>" "28850," + str(0.90*unload_disp[ii + i * 10, 0]-0.005) + "</loadpoint>\n")
        # f.write("\t\t\t<loadpoint>" "28950," + str(0.65*0.01) + "</loadpoint>\n")
        f.write("\t\t\t<loadpoint>" "28890, 0.01" + "</loadpoint>\n")
        f.write("\t\t</loadcurve>\n")
        f.write("\t</LoadData>\n")
        f.write("</febio_spec>")
        f.close()

        f = open("parameters.feb", "w+")
        f.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n")
        f.write("<febio_spec version=\"2.5\">\n")
        f.write("\t<Parameters>\n")
        f.write("\t\t<param name=\"young_stroma\">"+str(E_stroma)+"</param>\n")
        f.write("\t\t<param name=\"permeability_stroma\">"+str(k_stroma)+"</param>\n")
        f.write("\t\t<param name=\"poison_stroma\">" + str(nu_stroma) + "</param>\n")
        f.write("\t\t<param name=\"young_epi\">" + str(E[3]) + "</param>\n")
        f.write("\t\t<param name=\"permeability_epi\">" + str(k[2]) + "</param>\n")
        f.write("\t\t<param name=\"poison_epi\">" + str(nu) + "</param>\n")
        f.write("\t\t<param name=\"poison_epi\">" + str(nu) + "</param>\n")
        if i == 0:
            f.write("\t\t<param name=\"M_epi\">" + str(0) + "</param>\n")
            f.write("\t\t<param name=\"alpha_epi\">" + str(0) + "</param>\n")
            f.write("\t\t<param name=\"M_stroma\">" + str(M_stroma[ii]) + "</param>\n")
            f.write("\t\t<param name=\"alpha_stroma\">" + str(alpha_stroma[4]) + "</param>\n")
        if i == 1:
            f.write("\t\t<param name=\"M_epi\">" + str(0) + "</param>\n")
            f.write("\t\t<param name=\"alpha_epi\">" + str(0) + "</param>\n")
            f.write("\t\t<param name=\"M_stroma\">" + str(M_stroma[4]) + "</param>\n")
            f.write("\t\t<param name=\"alpha_stroma\">" + str(alpha_stroma[ii]) + "</param>\n")
        if i == 2:
            f.write("\t\t<param name=\"M_epi\">" + str(M[ii]) + "</param>\n")
            f.write("\t\t<param name=\"alpha_epi\">" + str(alpha[4]) + "</param>\n")
            f.write("\t\t<param name=\"M_stroma\">" + str(M_stroma[-2]) + "</param>\n")
            f.write("\t\t<param name=\"alpha_stroma\">" + str(alpha_stroma[-3]) + "</param>\n")
        if i == 3:
            f.write("\t\t<param name=\"M_epi\">" + str(M[4]) + "</param>\n")
            f.write("\t\t<param name=\"alpha_epi\">" + str(alpha[ii]) + "</param>\n")
            f.write("\t\t<param name=\"M_stroma\">" + str(M_stroma[-2]) + "</param>\n")
            f.write("\t\t<param name=\"alpha_stroma\">" + str(alpha_stroma[-3]) + "</param>\n")
        f.write("\t</Parameters>\n")
        f.write("</febio_spec>")
        f.close()
        if True:  # i != 1 or ii == 7:
            os.system("febio2 -i parameter_study.feb")
            os.system("mkdir /mnt/BackUp/jeremy/repository/keratometry/Python/first_model_evaluatiion/parameter_study/dir"+str(i)+str(ii))
            os.system(("mv anterior_surface_displacement.dat /mnt/BackUp/jeremy/repository/keratometry/Python/first_model_evaluatiion/parameter_study/dir"+str(i)+str(ii)))
            os.system("mkdir dir" + str(i) + str(ii))
            os.system(("cp parameter_study.xplt dir" + str(i) + str(ii) + "/parameter_study.xplt"))
            os.system(("cp parameters.feb dir" + str(i) + str(ii) + "/parameters.feb"))

