import os

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from my_functions import *
from scipy.io import loadmat


feb_file = {'febio': {'parameter study': 'abaqus_mesh.feb'}}
nodes_disp_file = {'parameter study': 'anterior_surface_displacement.dat'}

fig1, ax1 = plt.subplots()
fig1.suptitle('Epi: M = 0; alpha = 0; Stroma: alpha= 0.06: change in stroma M', Fontsize=16)
fig2, ax2 = plt.subplots()
fig2.suptitle('Epi: M = 0; alpha = 0; Stroma: M = 3.2: change in stroma alpha', Fontsize=16)
fig3, ax3 = plt.subplots()
fig3.suptitle('Epi:alpha = 0.06; Stroma: M = 6.2; alpha = 1.3: change in epithelium M', Fontsize=16)
fig4, ax4 = plt.subplots()
fig4.suptitle('Epi: M = 3.2; Stroma: M = 6.2; alpha = 1.3:: change in epithelium alpha', Fontsize=16)
fig5, ax5 = plt.subplots()
fig5.suptitle('4 days Epi: k = 4e-6 mm⁴/(Ns)Stroma: k = 0.00085 mm⁴/(Ns)', Fontsize=16)

section = ['<Nodes name="Cornea"', '<NodeSet name="anterior_surface">']
file = list(nodes_disp_file.keys())[0]
M = np.linspace(0.2, 7, 10)
alpha = np.logspace(-3, 1, 10)

l_name = np.ndarray.tolist(M) + np.ndarray.tolist(alpha) + np.ndarray.tolist(M) + np.ndarray.tolist(alpha) + ['4 cycles']

disp_8h = np.zeros([50, 1])
for f_num in feb_file:
    for k in range(40, 41):
        nodes = load_feb_file_nodes(feb_file[f_num][file], section[0])
        nodes_index = load_feb_file_nodes_id(feb_file[f_num][file], section[1])
        if k < 10:
            path2file = os.path.join('dir'+"0"+str(k), nodes_disp_file[file])
            t, nodes_disp = load_output_dat_file(path2file)

        else:
            path2file = os.path.join('dir' + str(k), nodes_disp_file[file])
            t, nodes_disp = load_output_dat_file(path2file)

        nodes = np.asarray(nodes)
        nodes_index = np.asarray(nodes_index)
        nodes_disp = np.reshape(np.asarray(nodes_disp), (len(t), len(nodes_index)*4), order='C')
        t = np.asarray(t)
        x = np.zeros((len(nodes_index), 3*len(t)))
        iii = 0
        # stitch disp and pos together
        for steps in t:
            ii = 0
            for i in nodes_index:
                temp = nodes[np.where(nodes[:, 0] == i)[0], :] + nodes_disp[iii, int(np.where(nodes_disp[iii, :] == i)[0]): int(np.where(nodes_disp[iii, :] == i)[0]) + 4]
                x[ii, iii * 3:(iii + 1) * 3] = temp[0, 1:]
                ii += 1
            iii += 1
            if steps == 28800:
                disp_8h[k] = np.min(nodes_disp)
        x = np.array(sorted(x, key=lambda x_column: x_column[0]))

        # revovle for spherical/biconical fit
        rot_angle = 5 / 180 * np.pi
        slices = int(2 * np.pi / rot_angle)
        skip = 10  # at least one
        index_15 = (np.abs(x[:, 0] - 1.5)).argmin()
        x_revolved = np.zeros([np.int(np.ceil(index_15 / skip)) * slices, 3 * len(t)])
        for jj in range(slices):
            R_y = np.matrix(
                [[np.round(np.cos(jj * rot_angle), decimals=6), 0, np.round(np.sin(jj * rot_angle), decimals=6)],
                 [0, 1, 0],
                 [-np.round(np.sin(jj * rot_angle), decimals=6), 0, np.round(np.cos(jj * rot_angle), decimals=6)]])
            for j in range(len(t)):
                # biconic fitting describes surface in function of z; turn coordinate system accordingly
                temp = np.transpose(np.dot(R_y, np.transpose(x[0:index_15:skip, j * 3:(j + 1) * 3])))
                temp[:, 1] = -(temp[:, 1] - np.max(temp[:, 1], axis=0))
                x_revolved[jj * np.int(np.ceil(index_15/skip)):(jj + 1)*np.int(np.ceil(index_15/skip)), j*3:(j + 1)*3] = np.concatenate([temp[:, 2], temp[:, 0], temp[:, 1]], axis=1)

        # calculate Radius and power of the eye
        n = 1.3375
        R_n = np.zeros(np.array([len(t), 1]))
        R_ny = np.zeros(np.array([len(t), 1]))
        power_eye = np.zeros(np.array([len(t), 1]))
        R = np.zeros(np.array([len(t), 1]))
        for j in range(int(np.float(len(t)))):
            pos = x_revolved[:, j * 3:(j + 1) * 3]
            r = sphere_fit(pos)
            # r = biconic_fitting(pos)
            R_n[j] = r[0]
            R_ny[j] = r[1]
            R[j] = (R_n[j] + R_ny[j])/2
            power_eye[j] = (n - 1) / (R_n[j] * 1e-3)
        # plot radius eye
        # ax1 = plt.subplot(211)
        if k < 10:
            label_name = str(l_name[k])
            ax1.plot(t / 3600, power_eye - (n - 1) / 0.0076, label=label_name)
            leg = ax1.legend(loc='lower right', fontsize=9)
            ax1.set_xlabel('time [h]', Fontsize=12)
            ax1.set_ylabel('refractive power change [D]', Fontsize=12)
            # plt.ylim([7.6, 8.2])
            plt.xticks((np.arange(0, 22, 2)))
        elif k < 20:
            label_name = str(l_name[k])
            ax2.plot(t / 3600, power_eye - (n - 1) / 0.0076, label=label_name)
            leg = ax2.legend(loc='lower right', fontsize=9)
            ax2.set_xlabel('time [h]', Fontsize=12)
            ax2.set_ylabel('refractive power change [D]', Fontsize=12)
            # plt.ylim([7.6, 8.2])
            plt.xticks((np.arange(0, 22, 2)))
        elif k < 30:
            label_name = str(l_name[k])
            # ax2.plot(t / 3600, R_n, label=label_name)
            ax3.plot(t / 3600, power_eye - (n - 1) / 0.0076, label=label_name)
            leg = ax3.legend(loc='lower right', fontsize=9)
            ax3.set_xlabel('time [h]', Fontsize=12)
            ax3.set_ylabel('refractive power change [D]', Fontsize=12)
            # plt.ylim([7.6, 8.2])
            plt.xticks((np.arange(0, 22, 2)))
        elif k < 40:
            label_name = str(l_name[k])
            # ax2.plot(t / 3600, R_n, label=label_name)
            ax4.plot(t / 3600, power_eye - (n - 1) / 0.0076, label=label_name)
            leg = ax4.legend(loc='lower right', fontsize=9)
            ax4.set_xlabel('time [h]', Fontsize=12)
            ax4.set_ylabel('refractive power change [D]', Fontsize=12)
            # plt.ylim([7.6, 8.2])
            plt.xticks((np.arange(0, 22, 2)))
        else:
            label_name = str(l_name[k])
            # ax2.plot(t / 3600, R_n, label=label_name)
            ax5.plot(t / 3600, power_eye - (n - 1) / 0.0076, label=label_name)
            leg = ax5.legend(loc='lower right', fontsize=9)
            ax5.set_xlabel('time [h]', Fontsize=12)
            ax5.set_ylabel('refractive power change [D]', Fontsize=12)
            # plt.ylim([7.6, 8.2])
            plt.xticks((np.arange(0, 96, 2)))

    ax1.plot([0, 22], [-1.74, -1.74], color='black', lw=1)
    ax1.plot([16, 16], [0, -3], color='black', lw=1)
    ax2.plot([0, 22], [-1.74, -1.74], color='black', lw=1)
    ax2.plot([16, 16], [0, -3], color='black', lw=1)
    ax3.plot([0, 22], [-1.74, -1.74], color='black', lw=1)
    ax3.plot([16, 16], [0, -3], color='black', lw=1)
    ax4.plot([0, 22], [-1.74, -1.74], color='black', lw=1)
    ax4.plot([16, 16], [0, -3], color='black', lw=1)
    ax5.plot([0, 96], [-1.74, -1.74], color='black', lw=1)
    ax5.plot([16, 16], [0, -3], color='black', lw=1)
    ax5.plot([40, 40], [0, -3], color='black', lw=1)
    ax5.plot([64, 64], [0, -3], color='black', lw=1)
    ax5.plot([88, 88], [0, -3], color='black', lw=1)

    fig1.show()