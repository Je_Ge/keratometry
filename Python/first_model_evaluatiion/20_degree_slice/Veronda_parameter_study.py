import numpy as np
import os
import matplotlib as plt
import pickle
from copy import deepcopy
from my_functions import *


ite_max = 10  # [-]
tol_error = 5e-4  # [mm]


main_path = '/mnt/BackUp/jeremy/FEBIO/20_degr_model'
path_2nd = '/mnt/BackUp/jeremy/repository/keratometry/Python/first_model_evaluatiion/20_degree_slice'
os.chdir(main_path)  # get and create file in FEBio folder
os.system('cp geometry_init.feb geometry_opt.feb')
parameters = [6.719636572567563e-05, 41.333333333333336, 100, 0.002, 0.075, 0.00085, 0, 0, 0, 0, 4.6e-6, 0 , 0, 0, 0]
parameter_name = ['c_1', 'c_2', 'k', 'E_epi', 'nu_epi', 'permeability_stroma', 'gamma_stroma', 'tau_stroma', 'M_stroma',
                  'alpha_stroma', 'permeability_epi', 'M_epi', 'alpha_epi', 'gamma_epi', 'tau_epi']
apical_rise_fem = {}
for j in range(1):
    for jj in range(1):
        write_parameters(parameters, parameter_name, path=main_path)
        pre_stretch(ite_max, tol_error, path=main_path)
        # path_temp = os.path.join(main_path, 'dir' + str(j) + str(jj))
        # os.system('mkdir ' + path_temp)
        # os.system('cp geometry_opt.feb ' + path_temp + '/geometry_opt.feb')
        os.system('febio2 -i 1_day_with_prestretch.feb')
        dat_path = os.path.join(path_2nd, 'dir' + str(j) + str(jj))
        os.system('mkdir ' + dat_path)
        os.system('mv anterior_surface.dat ' + dat_path)