import numpy as np
import os
import matplotlib as plt
from copy import deepcopy
from my_functions import *

### Cornea 3D-slice pre-stretch ###

ite_max = 10            # [-]
tol_error = 1e-4        # [mm]
i = 0
error = np.inf          # [mm]
main_path = '/mnt/BackUp/jeremy/FEBIO/pre_stretch'
os.chdir(main_path)  # get and create file in FEBio folder
os.system('cp geometry_init.feb geometry_opt.feb')

X_aim = np.asarray(load_feb_file_nodes('geometry_init.feb', '<Nodes name=\"Cornea\">', path=main_path))
X_subopt = np.asarray(load_feb_file_nodes('geometry_opt.feb', '<Nodes name=\"Cornea\">', path=main_path))
X_opt = deepcopy(X_subopt)
X_opt[:, 1:] = 0.90*X_subopt[:, 1:]
write_febio_geometry_file('geometry_opt.feb', X_opt, path=main_path)
while (i < ite_max) and (error > tol_error):
    os.system('febio2 -i pre_stretch.feb')
    X_subopt = np.asarray(load_feb_file_nodes('geometry_opt.feb', '<Nodes name=\"Cornea\">', path=main_path))
    t, x = load_output_dat_file('disp_pre_stretch.dat', path=main_path)
    x = np.asarray(x)
    X_def = x[np.where(x[:, 0] == 1)[0][-1]:np.where(x[:, 0] == X_aim.shape[0])[0][-1]+1, :]
    X_error = X_aim[:, 1:] - X_def[:, 1:]
    error = np.max(np.abs(X_error))
    X_opt = deepcopy(X_def)
    X_opt[:, 1:] = X_error + X_subopt[:, 1:]
    write_febio_geometry_file('geometry_opt.feb', X_opt, path=main_path)
    print(i, error)
    i += 1
