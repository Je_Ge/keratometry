#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  1 15:30:05 2017

@author: mariza
"""

import os
from textRepr import *
from odbAccess import *
from operator import attrgetter as att
import numpy as np

class RetrieveABQ_data(object):

    def __init__(self, DataBase='', pathDataBase='', partName='PART-1-1',
                 variables=('U'), element_type='', position='CENTROID', elset_name='', nset_name=''):

# Path to database
        if pathDataBase is '':
            self.pathDataBase = os.getcwd()
        else:
            self.pathDataBase = pathDataBase

# Name of database and loading data base
        if '.' in DataBase:
            strName = DataBase.split('.')
            name = strName[0] + '.odb'
        else:
            name = DataBase + '.odb'
        self.loadDataBase = os.path.join(self.pathDataBase, name)

# Name of the part
        if isinstance(partName, str):
            self.PartName = partName
        else:
            print('Bad Part Name definition ("PART-1-1")')
            return

#Name of the step to retrieve
#        self.Step = step
#        self.Frame = frame

#Variables to print
        if isinstance(variables, tuple):
            self.Variables = variables
        else:
            print('Bad variables definition ("U", "S", "LE")')
            return

#Element type
        if element_type is not '':
            self.elementType=element_type
        else:
            self.elementType=''
            print('Element Type is empty')

#Element point
        if position.lower() == 'centroid' or position.lower() == 'integration_point':
            self.elPosition=position.upper()
        else:
            print('Bad Position definition (CENTROID/INTEGRATION_POINT)')
            return

#Element set
        if elset_name is not '':
            self.elset_name = elset_name
        else:
            self.elset_name = ''
            print('Elset group is empty')

#Node set
        if nset_name is not '':
            self.nset_name = nset_name
        else:
            self.nset_name = ''
            print('Nset group is empty')

#Store information
        self.geometry_tree = type('object', (), {})() #Dynamic Object for Storing
        self.results_tree = type('object', (), {})() #Dynamic Object for Storing

## PRETTY PRINT    
    def pretty_report(self, verbosity=1):

        myOdb = openOdb(path=self.loadDataBase)
        prettyPrint(myOdb,verbosity)

## GET FINITE ELEMENT MODEL (GEOMETRY; LABELS; ETC)
    def get_FEM_model(self):
        global myElsetLabels, myElsetLabels

	myOdb = openOdb(path=self.loadDataBase) # Opend ODB data base

        nameInstance = myOdb.rootAssembly.instances.keys()
        if len(nameInstance)>1:
            print('Exists more than 1 part in the model')
            for names in nameInstance:
                print(names, )
            nameInstance = nameInstance[input('Select desired part (0, 1 ...): ')]
            self.PartName = nameInstance
        else:
            if self.PartName is not nameInstance:
                self.Partname = nameInstance

        myInstance = myOdb.rootAssembly.instances[self.PartName] # Load Instance

        myNodes = myInstance.nodes
        nLabels = np.array(map(att('label'), myNodes), dtype=int)
        nCoords = np.array(map(att('coordinates'), myNodes))
        Nodes = np.column_stack((nLabels, nCoords))
        self.geometry_tree.Nodes = Nodes #Store nodes (labels, coordX, coordY, coordZ)

        print('Total nodes of the model: %d' % Nodes.shape[0])

        myElements = myInstance.elements
        self.elementType = myElements[0].type
        eLabels = np.array(map(att('label'), myElements), dtype=int)
        eConnec = np.array(map(att('connectivity'), myElements))
        Elements = np.column_stack((eLabels, eConnec))
        #Elements = eLabels
        self.geometry_tree.Elements = Elements

        print('Total elements of the model: %d' % Elements.shape[0])

        if self.elset_name is not '':
            NameSets = self.elset_name
            if NameSets in myInstance.elementSets.keys():
                myElset = myInstance.elementSets[NameSets].elements
                myElsetLabels = np.array(map(att('label'), myElset), dtype=int)
                self.geometry_tree.myElsetLabels = myElsetLabels
                self.geometry_tree.myElset = myInstance.elementSets[NameSets]
                print('Total group elements(%s): %d' % (self.elset_name, max(myElsetLabels.shape)))
                print(myInstance.elementSets.keys())
            else:
                print('Unknown Elset Label')
		print(myInstance.elementSets.keys())
                return

        if self.nset_name is not '':
            NameSets = self.nset_name
	    print('Nset Groups:')
            print(myInstance.nodeSets.keys())
            if NameSets in myInstance.nodeSets.keys():
                myNset = myInstance.nodeSets[NameSets].nodes
                myNsetLabels = np.array(map(att('label'), myNset), dtype=int)
                self.geometry_tree.myNsetLabels = myNsetLabels
                self.geometry_tree.myNset = myInstance.nodeSets[NameSets]
                #myNsetCoords = np.array(map(att('coordinates'), myNset))
                print('Total group nodes (%s): %d' %(self.nset_name, max(myNsetLabels.shape)))
            else:
                print('Unknown Nset Label')
                return

        if self.nset_name is not '' and self.elset_name is '':
            return Nodes, Elements, myNsetLabels

        if self.elset_name is not '' and self.nset_name is '':
            return Nodes, Elements, myElsetLabels

        if self.elset_name is not '' and self.nset_name is not '':
            return Nodes, Elements, myNsetLabels, myElsetLabels

        if self.elset_name is '' and self.nset_name is '':
            return Nodes, Elements

    def get_last_computation(self):

        myOdb = openOdb(path=self.loadDataBase)

        stepLabels = myOdb.steps.keys()

        lastFrame = myOdb.steps[stepLabels[-1]].frames[-1]

        for var in self.Variables:
            if var in ('U', 'S', 'LE'):
                if var.upper() == 'U':
                    if self.nset_name is '':
                        fieldValues = lastFrame.fieldOutputs[var.upper()].values
                        vector = np.array(map(att('data'), fieldValues))
                        magnitude = np.array(map(att('magnitude'), fieldValues))
                        self.results_tree.U = np.column_stack((vector, magnitude))
                    else:
                        fieldValues = lastFrame.fieldOutputs[var.upper()]
                        fieldSubset = fieldValues.getSubset(region=self.geometry_tree.myNset)
                        fieldSubsetValues = fieldSubset.values
                        vector = np.array(map(att('data'), fieldSubsetValues))
                        magnitude = np.array(map(att('magnitude'), fieldSubsetValues))
                        self.results_tree.U = np.column_stack((vector, magnitude))

                else:
                    if self.elset_name is '':
                        field = lastFrame.fieldOutputs[var.upper()]
                        fieldValues = field.values
                        tensor = np.array(map(att('data'), fieldValues))
                    else:
                        field = lastFrame.fieldOutputs[var.upper()]
                        fieldSubset = field.getSubset(region=self.geometry_tree.myElset,
                                                            position=self.elPosition, elementType=self.elementType)
                        fieldValues = fieldSubset.values
                        tensor = np.array(map(att('data'), fieldValues))
                    if var == 'S':
                        self.results_tree.S = tensor
                    elif var == 'LE':
                        self.results_tree.LE = tensor
            else:
                print('Unknown Variable: %s' % var)

        return self.results_tree
    
    def get_step_computation(self, step=-1, frame=-1):

        myOdb = openOdb(path=self.loadDataBase)

        stepLabels = myOdb.steps.keys()

        lastFrame = myOdb.steps[stepLabels[step]].frames[frame]

        for var in self.Variables:
            if var in ('U', 'S', 'LE'):
                if var.upper() == 'U':
                    if self.nset_name is '':
                        fieldValues = lastFrame.fieldOutputs[var.upper()].values
                        vector = np.array(map(att('data'), fieldValues))
                        magnitude = np.array(map(att('magnitude'), fieldValues))
                        self.results_tree.U = np.column_stack((vector, magnitude))
                    else:
                        fieldValues = lastFrame.fieldOutputs[var.upper()]
                        fieldSubset = fieldValues.getSubset(region=self.geometry_tree.myNset)
                        fieldSubsetValues = fieldSubset.values
                        vector = np.array(map(att('data'), fieldSubsetValues))
                        magnitude = np.array(map(att('magnitude'), fieldSubsetValues))
                        self.results_tree.U = np.column_stack((vector, magnitude))

                else:
                    if self.elset_name is '':
                        field = lastFrame.fieldOutputs[var.upper()]
                        fieldValues = field.values
                        tensor = np.array(map(att('data'), fieldValues))
                    else:
                        field = lastFrame.fieldOutputs[var.upper()]
                        fieldSubset = field.getSubset(region=self.geometry_tree.myElset,
                                                            position=self.elPosition, elementType=self.elementType)
                        fieldValues = fieldSubset.values
                        tensor = np.array(map(att('data'), fieldValues))
                    if var == 'S':
                        self.results_tree.S = tensor
                    elif var == 'LE':
                        self.results_tree.LE = tensor
            else:
                print('Unknown Variable: %s' % var)

        return self.results_tree


    def get_allSteps(self, mode='implicit'):

        if mode == 'implicit':
            data_type = 'data'
        elif mode == 'explicit':
            data_type = 'dataDouble'
        else:
            raise ValueError('Use implicit or explicit')

        myOdb = openOdb(path=self.loadDataBase)

        stepLabels = myOdb.steps.keys()

        for s in stepLabels:

            Frames = myOdb.steps[s].frames
            nFrames = len(Frames)
            output_displacement = []
            output_stress = []
            output_stretch = []

            for ff in range(0, nFrames):
                lastFrame = Frames[ff]
		time = Frames[ff].frameValue
                # Extract displacement
                var = 'U'
                if self.nset_name is '':
                    fieldValues = lastFrame.fieldOutputs[var.upper()].values
                    vector = np.array(map(att(data_type), fieldValues))
		    label = np.array(map(att('nodeLabel'), fieldValues))
                else:
                    fieldValues = lastFrame.fieldOutputs[var.upper()]
                    fieldSubset = fieldValues.getSubset(region=self.geometry_tree.myNset)
                    fieldSubsetValues = fieldSubset.values
                    vector = np.array(map(att(data_type), fieldSubsetValues))
                    label = np.array(map(att('nodeLabel'), fieldSubsetValues))
                aux = np.c_[label, vector]
                output_displacement.append(aux)
                
                # Extract stress
                var = 'S'
                if self.elset_name is '':
                    fieldValues = lastFrame.fieldOutputs[var.upper()].values
                    vector = np.array(map(att(data_type), fieldValues))
		    label = np.array(map(att('elementLabel'), fieldValues))
                else:
                    fieldValues = lastFrame.fieldOutputs[var.upper()]
                    fieldSubset = fieldValues.getSubset(region=self.geometry_tree.myElset)
                    fieldSubsetValues = fieldSubset.values
                    vector = np.array(map(att(data_type), fieldSubsetValues))
                    label = np.array(map(att('elementLabel'), fieldSubsetValues))
                aux = np.c_[label, vector]
                output_stress.append(aux)
                
                # Extract stress
                var = 'LE'
                if self.elset_name is '':
                    fieldValues = lastFrame.fieldOutputs[var.upper()].values
                    vector = np.array(map(att(data_type), fieldValues))
		    label = np.array(map(att('elementLabel'), fieldValues))
                else:
                    fieldValues = lastFrame.fieldOutputs[var.upper()]
                    fieldSubset = fieldValues.getSubset(region=self.geometry_tree.myElset)
                    fieldSubsetValues = fieldSubset.values
                    vector = np.array(map(att(data_type), fieldSubsetValues))
                    label = np.array(map(att('elementLabel'), fieldSubsetValues))
                aux = np.c_[label, vector]
                output_stretch.append(aux)
                

        return output_displacement, output_stress, output_stretch
