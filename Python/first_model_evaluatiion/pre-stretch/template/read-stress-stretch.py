#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  1 15:47:04 2017

@author: mariza

Usage: abaqus python launcher.py -odb odbName -step (optional) stepNumber -frame (optional) frameNumber
                                 -part (optional) partName -elset(optional) elsetName -nset (optional) nsetName
                                 -variables (optional) (U, S, LE) -location (optional) Centroid
Requirements:
-odb   : Name of the output database.
-step  : Step that wants to be retrieved
-frame : Frame within the step that wants to be retrieved (usually 0 -initial- or -1 -final-)
-part  : Part name that wants to be analysed
-elset : Name of the assembly level element set.
            Search will be done only for element belonging
            to this set. If this parameter is not provided,
            search will be performed over the entire model.
-nset  : Name of the assembly level node set.
            Search will be done only for element belonging
            to this set. If this parameter is not provided,
            search will be performed over the entire model.
-variables : variables that want to be retrieved (currently U, S, LE)
-location: location in the element ('CENTROID')
-help  : Print usage
"""
import os
import numpy as np
import pyAbaq as rFEMdat
from sys import argv,exit
import time

start_time = time.time()

odbName = None
partName = None
elsetName = None
nsetName = None
step_k = None
frame_k = None
location = None
argList = argv
argc = len(argList)

i=0
while (i < argc):
    if (argList[i][:2] == "-o"):
        i += 1
        odbName = argList[i]
    elif (argList[i][:2] == "-s"):
        i += 1
        step_k = argList[i]
    elif (argList[i][:2] == "-f"):
        i += 1
        frame_k = argList[i]
    elif (argList[i][:2] == "-p"):
        i += 1
        partName = argList[i]
	partName = partName.upper()
    elif (argList[i][:2] == "-e"):
        i += 1
        elsetName = argList[i]
	elsetName = elsetName.upper()
    elif (argList[i][:2] == "-n"):
        i += 1
        nsetName = argList[i]
	nsetName = nsetName.upper()
    elif (argList[i][:2] == "-l"):
        i += 1
        location = argList[i]
	location = location.upper()
    elif (argList[i][:2] == "-h"):
        print(__doc__)
        exit(0)
    i += 1
    
if not (odbName):
    print(' **ERROR** output database name is not provided')
    print(__doc__)
    exit(1)

if elsetName is None:
    elsetName = ''

if nsetName is None:
    nsetName = ''
    
if step_k is None:
    step_k = -1
else:
    step_k = int(step_k)

if frame_k is None:
    frame_k = -1
else:
    frame_k = int(frame_k)

if location is None:
    location = 'CENTROID'

if partName is None:
    partName = 'PART-1-1'
        
#print(odbName, partName, step_k, frame_k, nsetName, elsetName, location)
print('Odb Name: ', odbName)
print('Part Name: ', partName)
print('Step: ', step_k)
print('Frame: ', frame_k)
print('Nset Name: ', nsetName)
print('Elset Name: ', elsetName)
print('Location: ', location)

##Retrieve data    
#(DataBase='', pathDataBase='', partName='PART-1-1', step=-1, variables=('U'), element_type='', position='CENTROID', elset_name='', nset_name='')
currFEM = rFEMdat.RetrieveABQ_data(DataBase=odbName, partName=partName,
                                   variables=('U', ), nset_name=nsetName, elset_name=elsetName)
#currFEM.pretty_report(verbosity=1)

# Retrieve Geometry
file_nodes = 'nodes_'+odbName+'_'+nsetName+'.dat'
file_elements = 'elements_'+odbName+'_'+nsetName+'.dat'
file_nsets = 'nsets_'+odbName+'_'+nsetName+'.dat'
file_elsets = 'elsets_'+odbName+'_'+nsetName+'.dat'
if (nsetName is not '') and (elsetName is not ''):
    nodes, elements, nsets, elsets = currFEM.get_FEM_model()
    np.savetxt(os.path.join(os.getcwd(),file_nodes), nodes, fmt='%d %3.9f %3.9f %3.9f')
    np.savetxt(os.path.join(os.getcwd(),file_nsets), nsets, fmt='%d')
    np.savetxt(os.path.join(os.getcwd(),file_elsets), elsets, fmt='%d')
    
if (nsetName is not '') and (elsetName is ''):
    nodes, elements, nsets = currFEM.get_FEM_model()
    np.savetxt(os.path.join(os.getcwd(),file_nodes), nodes, fmt='%d %3.9f %3.9f %3.9f')
    np.savetxt(os.path.join(os.getcwd(),file_nsets), nsets, fmt='%d')

if (nsetName is '') and (elsetName is not ''):
    print('caca')
    nodes, elements, elsets = currFEM.get_FEM_model()
    np.savetxt(os.path.join(os.getcwd(),file_nodes), nodes, fmt='%d %3.9f %3.9f %3.9f')
    np.savetxt(os.path.join(os.getcwd(),file_elsets), elsets, fmt='%d')
    
if (nsetName is '') and (elsetName is ''):
    nodes, elements = currFEM.get_FEM_model()
    if nodes.shape[1] == 3:
        nodes = np.c_[nodes, np.zeros(nodes.shape[0])]
    np.savetxt(os.path.join(os.getcwd(),file_nodes), nodes, fmt='%d %3.9f %3.9f %3.9f')

    fmt_list = ['%d' for x in range(elements.shape[1])]
    print(fmt_list)
    #np.savetxt(os.path.join(os.getcwd(),file_elements), elements, fmt='%d %d %d %d %d')
    np.savetxt(os.path.join(os.getcwd(),file_elements), elements, fmt=fmt_list)

##Retrieve all frames
import pickle
U, S, LE = currFEM.get_allSteps()
pickle.dump(U, open('U.pkl', 'wb'), protocol=-1)
pickle.dump(S, open('S.pkl', 'wb'), protocol=-1)
pickle.dump(LE, open('LE.pkl', 'wb'), protocol=-1)

print('Execution time: %f seconds' %(time.time() - start_time))
